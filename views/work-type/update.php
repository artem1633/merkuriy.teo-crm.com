<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkType */
?>
<div class="work-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
