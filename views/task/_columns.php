<?php

use app\models\Contract;
use app\models\TaskStatus;
use app\models\WorkType;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_id',
        'content' => function($data){
            $contract = ArrayHelper::getColumn(Contract::find()->where(['id' => $data->contract_id])->all(), 'auction_number');
            return implode('',$contract);

        },
        'filter' => ArrayHelper::map(Contract::find()->asArray()->all(), 'id', 'auction_number'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'work_type_id',
        'content' => function($data){
            $workType = ArrayHelper::getColumn(WorkType::find()->where(['id' => $data->work_type_id])->all(), 'name');
            return implode('',$workType);

        },
        'filter' => ArrayHelper::map(WorkType::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'attribute' => 'task_status_id',
        'value' => function ($model, $key, $index, $widget) {
            $statusName = implode('',ArrayHelper::getColumn(TaskStatus::find()->where(['id' => $model->task_status_id])->all(), 'name'));
            $statusColor = implode('', ArrayHelper::getColumn(TaskStatus::find()->where(['id' => $model->task_status_id])->all(), 'color'));
            return "<div style='display: flex;background-color: {$statusColor};color: black'> <span>{$statusName}</span></div>";

        },
        'width' => '',
        'filter' => ArrayHelper::map(TaskStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => 'dropdown',
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'deadline',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'author_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   