<?php

use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
?>

<div class="task-view">
<?php Pjax::begin(['id'=>'pjax-container-info-container', 'enablePushState' => false]); ?>
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['task/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],
                        ['role'=>'modal-remote','title'=> 'Изменить Задачу','class'=>'btn btn-warning btn-xs'])?>
                </div>
            </div>
            <div class="panel-body"  style="height: 450px; overflow-y: auto;">
                <div class="row">
                    <div class="col-md-12">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                    'attribute' => 'contract_id',
                                    'value' => ArrayHelper::getValue($model->getContract()->one(),'auction_number')
                            ],
                            [
                                'attribute' => 'work_type_id',
                                'value' => ArrayHelper::getValue($model->getWorkType()->one(),'name')
                            ],
                            [
                                'attribute' => 'task_status_id',
                                'value' => ArrayHelper::getValue($model->getTaskStatus()->one(),'name')
                            ],
                            'name',
                            'date',
                            'deadline',
                            'created_at',
                            [
                                'attribute' => 'author_id',
                                'value' => ArrayHelper::getValue($model->getAuthor()->one(),'name')
                            ],
                        ],
                    ]) ?>
                    </div>

                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Календарь Задач</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>


                    <div class="panel-body" style="height: 350px">
                        <div class="col-md-12">
                            <?php $this->beginContent('@app/views/task/status.php',[
                                'model' => $model,
                            ]); ?>

                            <?php $this->endContent(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

</div>

<?php Pjax::end() ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>