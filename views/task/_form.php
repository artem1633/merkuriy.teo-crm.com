<?php

use app\models\Contract;
use app\models\TaskStatus;
use app\models\WorkType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(Contract::find()->all(), 'id', 'auction_number'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите Лот ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'work_type_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(WorkType::find()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите вид работ ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'task_status_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(TaskStatus::find()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите статус ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>



    <?= $form->field($model, 'date')->input('date') ?>

    <?= $form->field($model, 'deadline')->input('date') ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
