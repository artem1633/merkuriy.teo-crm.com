<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSpecification */
?>
<div class="product-specification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'count',
            'price',
            'sum',
            'product_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
