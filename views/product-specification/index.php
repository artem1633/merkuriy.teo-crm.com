<?php

use kartik\export\ExportMenu;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $candidateId integer */

CrudAsset::register($this);

?>

<?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'count',
        'price',
        'sum',
        ['class' => 'yii\grid\ActionColumn'],

    ];
echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => require(__DIR__.'/_columns.php'),
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Скачать',
            'class' => 'btn btn-success'
        ],
        'showColumnSelector' => false,
        'clearBuffers' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => false,
        ],
        'showConfirmAlert' => false,
        ]);

echo GridView::widget([
    'id'=>'specification-datatable',
    'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
    'pjax'=>true,
    'columns' => require(__DIR__.'/_columns.php'),
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'showPageSummary' => true,
]);

?>
