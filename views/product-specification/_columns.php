<?php
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'pageSummary' => true,
        'pageSummaryOptions' =>[
            'prepend' => 'Итого: '
        ],
        'content' => function($model){
            return "{$model->price}";
//            return "{$model->price} (".round($model->price / 318, 2)."%)";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
        'pageSummary' => true,
        'pageSummaryOptions' =>[
            'prepend' => 'Итого: '
        ],
        'content' => function($model){
            return "{$model->sum}";
//            return "{$model->sum} (".round($model->sum / 2253, 2)."%)";
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'product_id',
//    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],


];   