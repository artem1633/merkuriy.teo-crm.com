<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductSpecification */

?>
<div class="product-specification-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
