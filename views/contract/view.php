<?php

use app\models\Calculation;
use app\models\CargoStatus;
use app\models\Commission;
use app\models\ContractType;
use app\models\Control;
use app\models\Customer;
use app\models\OrderStatus;
use app\models\Payments;
use app\models\StartMaxPrice;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Contract */

$contract_id = $model->id;
$calculation_id = Calculation::find()->where(['contract_id' => $model->id])->one()->id;
$control_id = Control::find()->where(['contract_id' => $model->id])->one()->id;
$orderStatus = OrderStatus::find()->where(['contract_id' => $model->id])->one();
$orderStatus_id = $orderStatus->id;
$cargoStatus = CargoStatus::find()->where(['contract_id' => $model->id])->one();
$customer = Customer::find()->where(['contract_id' => $model->id])->one();
$commission_id = Commission::find()->where(['contract_id' => $model->id])->one()->id;
$startMaxPrice_id = StartMaxPrice::find()->where(['contract_id' => $model->id])->one()->id;
$cType = ArrayHelper::getValue(ContractType::find()->where(['id' => $model->contract_type_id])->one(),'name');

$lot_link_portal = $customer->lot_link_portal;
$lot_link = $customer->lot_link;
$link_1s = $customer->link_1s;
$lot_link_playground = $customer->lot_link_playground;

if ($orderStatus->name == 'Подписали') {
    if ($customer->date_type == 1) {
        $delivery_deadline = new DateTime("$orderStatus->date");
        $delivery_deadline->modify("+$customer->delivery_days day");
        $delivery_deadline = $delivery_deadline->format('d-m-Y');
    } elseif ($customer->date_type == 3) {
        $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($orderStatus->date, $customer->delivery_days)));
    } elseif ($customer->date_type == 2) {
        $delivery_deadline = $customer->delivery_date;
    } else {
        $delivery_deadline = 'Необходимо выбрать тип срока поставки';
    }
}else{
//    $delivery_deadline = 'Необходимо установить статус заказаза "Заключен"';
    if($orderStatus->name == 'Заключен'){
        $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($orderStatus->date, $customer->delivery_days)));
    } else {
        $delivery_deadline = 'Необходимо установить статус заказаза "Заключен"';
    }
}

if ($cargoStatus->name == 'Получен заказчиком'){
    if ($customer->pay_type == 1) {
        $pay_deadline = new DateTime("$cargoStatus->date");
        $pay_deadline->modify("+$customer->pay_days day");
        $pay_deadline = $pay_deadline->format('d-m-Y');
    } elseif ($customer->pay_type == 3) {
        $pay_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($cargoStatus->date, $customer->pay_days)));
    } elseif ($customer->pay_type == 2) {
        $pay_deadline = $customer->pay_date;
    } else {
        $pay_deadline = 'Необходимо выбрать тип срока оплаты';
    }
}else{
    $pay_deadline = 'Необходимо установить статус заказаза "Получен заказчиком"';
}


?>
<div class="contract-view" style="margin-top: -22px;margin-bottom: 30px;">
    <div class="main-text-wrapper" style="display: flex;justify-content: space-between;>
        <div class="info-box">
          <div class="info-wrapper">
              <h3><?=$model->auction_number ?>  (<?=$customer->short_name?>)  <?=$cType?></h3>
              <p>Проведение аукциона: <?=$customer->auction_date?></p>
              <p>Адрес доставки: <?=$customer->shipping_address?></p>
              <p>Срок поставки до: <?=$delivery_deadline?></p>
              <p>Срок оплаты до: <?=$pay_deadline?></p>
          </div>
    <div class="link-wrapper">
        <?php if ($lot_link != "" && $lot_link !== null):?>
            <div class="link-box">
                <span  title="<?=$lot_link?>" style="cursor: pointer; color: green; font-size: 30px; margin-left: 15px;margin-top: 10px;" onclick="copyFunction2();"><i class="fa fa-copy"></i></span>
                <a href="<?=$lot_link?>"target="_blank">Ссылка на Лот в системе поиска аукционов</a>

            </div>
        <?php endif;?>
        <?php if ($lot_link_playground != "" && $lot_link_playground !== null):?>
            <div class="link-box">
                <span  title="<?=$lot_link_playground?>" style="cursor: pointer; color: green; font-size: 30px; margin-left: 15px;margin-top: 10px;" onclick="copyFunction4();"><i class="fa fa-copy"></i></span>
                <a href="<?=$lot_link_playground?>"target="_blank">Ссылка на лот в на площадке</a>
            </div>
        <?php endif;?>
        <?php if ($lot_link_portal != "" && $lot_link_portal !== null):?>
        <div class="link-box">
             <span  title="<?=$lot_link_portal?>" style="cursor: pointer; color: green; font-size: 30px; margin-left: 15px;margin-top: 10px;" onclick="copyFunction1();"><i class="fa fa-copy"></i></span>
            <a href="<?=$lot_link_portal?>"target="_blank">Ссылка на лот на портале госзакупок</a>
        </div>
        <?php endif;?>

        <?php if ($link_1s !== '' && $link_1s !== null):?>
        <div class="link-box">
            <span  title="<?=$link_1s?>" style="cursor: pointer; color: green; font-size: 30px; margin-left: 15px;margin-top: 10px;" onclick="copyFunction3();"><i class="fa fa-copy"></i></span>
            <a href="<?=$link_1s?>"target="_blank">Ссылка на 1с</a>
        </div>
        <?php endif;?>


        <script>
            function copyFunction1() {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val('<?=$lot_link_portal?>').select();
                document.execCommand("copy");
                $temp.remove();
            }
        </script>
        <script>
            function copyFunction2() {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val('<?=$lot_link?>').select();
                document.execCommand("copy");
                $temp.remove();
            }
        </script>
        <script>
            function copyFunction3() {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val('<?=$link_1s?>').select();
                document.execCommand("copy");
                $temp.remove();
            }
        </script>
        <script>
            function copyFunction4() {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val('<?=$lot_link_playground?>').select();
                document.execCommand("copy");
                $temp.remove();
            }
        </script>
    </div>
        </div>



    </div>


    <!-- Centered Tabs -->
    <div class="tabs">
        <ul id="items" class="nav nav-tabs nav-justified">
            <li class="active"><a data-toggle="tab"  href="#tabs-1">Вкладка первая</a></li>
            <li><a data-toggle="tab" href="#tabs-2">Вкладка вторая</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <div id="tabs-1" class="tab-contents active">
            <?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Сведения о проведении закупки</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="height: 450px; overflow-y: auto;">
                            <?= $this->render('@app/views/customer/_form.php',[
                                'model' => $customerSearchModel,
                                'containerPjaxReload' => '#pjax-container-info-container',
                            ]); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Начальная и максимальная цена контракта и Сумма контракта</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">

                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="height:600px;; overflow-y: auto;">
                              <div class="col-md-12">
                                  <?= $this->render('@app/views/start-max-price/_form',[
                                      'model' => $startMaxPriceSearchModel,
                                      'containerPjaxReload' => '#pjax-container-info-container',
                                      'costsSearchModel' => $costsSearchModel,
                                      'costsDataProvider' => $costsDataProvider,
                                      'contract_id' => $model->id
                                  ]); ?>


                        </div>

                    </div>
                </div>


            </div>
            <?php Pjax::end() ?>
        </div>
        <div id="tabs-2" class="tab-contents">
            <?php Pjax::begin(['id' => 'pjax-container-info-container-2', 'enablePushState' => false]); ?>
            <div class="row">

                <div class="col-md-6">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Статус заказа</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/order-status/_form',[
                                'model' => $orderStatusSearchModel,
                                'containerPjaxReload' => '#pjax-container-info-container',
                                'customer' => $customer,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Контактное лицо организации</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['customer-contact/create', 'contract_id' => $contract_id, 'containerPjaxReload' => '#pjax-container-info-container-2'],
                                    ['role'=>'modal-remote','title'=> 'Добавить контактное лицо организации','class'=>'btn btn-warning btn-xs'])?>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/customer-contact/index',[
                                'searchModel' => $customerContactSearchModel,
                                'dataProvider' => $customerContactDataProvider,
                                'contract_id' => $model->id,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Статус груза</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/cargo-status/_form',[
                                'model' => $cargoStatusSearchModel,
                                'containerPjaxReload' => '#pjax-container-info-container'
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Отправка груза</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['cargo-ship/create', 'contract_id' => $contract_id, 'containerPjaxReload' => '#pjax-container-info-container-2'],
                                    ['role'=>'modal-remote','title'=> 'Добавить Отправку груза','class'=>'btn btn-warning btn-xs'])?>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/cargo-ship/index',[
                                'searchModel' => $cargoShipSearchModel,
                                'dataProvider' => $cargoShipDataProvider,
                                'contract_id' => $model->id,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Товар</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['product/create', 'contract_id' => $contract_id, 'containerPjaxReload' => '#pjax-container-info-container-2'],
                                    ['role'=>'modal-remote','title'=> 'Добавить Товар','class'=>'btn btn-warning btn-xs'])?>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/product/index',[
                                'searchModel' => $productSearchModel,
                                'dataProvider' => $productDataProvider,
                                'contract_id' => $model->id,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Спецификация Товара</h4>
                            <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 200px; overflow-y: auto;">
                            <?= $this->render('@app/views/product-specification/index',[
                                'searchModel' => $specificationSearchModel,
                                'dataProvider' => $specificationDataProvider,
                                'contract_id' => $model->id,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php Pjax::end() ?>
        </div>

    </div>

</div>

</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

