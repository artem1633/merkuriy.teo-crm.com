<?php

use app\models\CargoStatus;
use app\models\OrderStatus;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\widgets\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Контракты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Фильтры</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['method' => 'POST']); ?>

                <div class="col-md-6">
                    <?= $form->field($searchModel, 'filterId')->widget(Select2::class, [
                        'data' => ArrayHelper::map(\app\models\ContractFilter::find()->all(), 'id', 'name'),
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => 'Выберите',
                        ],
                        'pluginEvents' => [
                            "select2:unselecting" => "function() { $('#delete-btn').hide(); }",
                            'change' => 'function(){
                                var id = $(this).val();
                                $.get("/contract-filter/view-ajax?id="+id, function(response){
                                    $("#contractsearch-filtername").val(response.name);
                                    $("#contractsearch-order_status").val(response.order_status);
                                    $("#contractsearch-deliverystatus").val(response.delivery_status);
                                    $("#contractsearch-shortorgname").val(response.shortorgname);
                                    $("#contractsearch-contract_type_id").val(response.contract_type_id);
                                    $("#contractsearch-auction_number").val(response.auction_number);
                                    $("#contractsearch-order_status").trigger("change");
                                    $("#contractsearch-contract_type_id").trigger("change");
                                });
                                
                                if(id != ""){
                                    $("#delete-btn").attr("href", "/contract/delete-filter?id="+id);
                                    $("#delete-btn").show();
                                } else {
                                    $("#delete-btn").hide();
                                }
                            }'
                        ],
                    ])->label('Фильтр') ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($searchModel, 'filterName')->textInput()->label('Наименование') ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($searchModel, 'order_status')->widget(Select2::class, [
//                        'data' => ArrayHelper::map(OrderStatus::find()->all(), 'name', 'name'),
                        'data' => [
//                            'Не задано' => 'Не задано',
                            'Подача заявок' => 'Подача заявок',
                            'Ждем торгов' => 'Ждем торгов',
                            'Ждем контракт' => 'Ждем контракт',
                            'Контракт размещен' => 'Контракт размещен',
                            'Протокол разногласий' => 'Протокол разногласий',
                            'Контракт размещен повторно' => 'Контракт размещен повторно',
                            'Подписали' => 'Подписали',
                            'Заключен' => 'Заключен',
                            'Проиграли' => 'Проиграли'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => 'Выберите',
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($searchModel, 'deliv_status')->widget(Select2::class, [
                        'data' => [
//                            'Не задано' => 'Не задано',
                            'Собирается' => 'Собирается',
                            'Готов к отправке' => 'Готов к отправке',
                            'Отправлен' => 'Отправлен',
                            'Получен заказчиком' => 'Получен заказчиком',
                            'Форс мажор' => 'Форс мажор',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => 'Выберите',
                            'multiple' => true,
                        ],
                    ])->label('Статус доставки') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($searchModel, 'contract_type_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(\app\models\ContractType::find()->all(), 'id', 'name'),
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => 'Выберите',
                            'multiple' => true,
                        ],
                    ]) ?>
                </div>

                <input type="hidden" name="action">


                <div class="col-md-12">
                    <?= Html::a('Очистить', ['contract/index'], ['class' => 'btn btn-warning']) ?>
                    <?php if($searchModel->filterId != null): ?>
                        <?= Html::a('Удалить фильтр', ['delete-filter', 'id' => $searchModel->filterId], ['id' => 'delete-btn', 'class' => 'btn btn-danger']) ?>
                    <?php else: ?>
                        <?= Html::a('Удалить фильтр', '', ['id' => 'delete-btn', 'class' => 'btn btn-danger', 'style' => 'display: none;']) ?>
                    <?php endif; ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'onclick' => '$("input[name=\"action\"]").val("filter");']) ?>
                    <?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>

    </div>
</div>








<div class="panel panel-inverse position-index">

    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Контракты</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
                'panelBeforeTemplate' =>   "<div class='row'><div class='col-md-12'>". Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                        ['role'=>'modal-remote','title'=> 'Добавить контракт','class'=>'btn btn-success', 'style' => 'float: left;']).'&nbsp;'.
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить', 'style' => 'float: left; margin-left: 5px;']) . $this->render('_search', [
                                'searchModel' => $searchModel,
                    ]) . '</div></div>',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
