<?php

use kartik\form\ActiveForm;

?>


<?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'POST', 'class' => 'form-inline', 'options' => ['style' => 'float: left; margin-left: 15px; margin-top: -22px;']]); ?>

    <?= $form->field($searchModel, 'shortOrgName')->textInput()->label('Поиск')?>

<?php ActiveForm::end() ?>

<?php

$script = <<< JS
$('#contractsearch-shortorgname').change(function(){
    $('#search-form').submit();
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>