<?php

use app\models\Bidding;
use app\models\CargoStatus;
use app\models\ContractType;
use app\models\Customer;
use app\models\OrderStatus;
use app\models\StartMaxPrice;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'checkboxOptions' => function($model){
            $status = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $model->id])->one(), 'name');
            if ($status != 'Проиграли'){
                return ['disabled' =>  true];
            }
        }
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_type_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(ContractType::find()->where(['id' => $data->contract_type_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(ContractType::find()->asArray()->all(), 'id', 'name'),
        'filterType' => 'dropdown',
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
        'width' => '10%',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'auction_number',
        'content' => function($data){
//            $status = ArrayHelper::getColumn(ContractType::find()->where(['id' => $data->auction_number])->all(), 'name');
            return \yii\helpers\Html::a($data->auction_number,['contract/view', 'id' => $data->id]);

        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fullName',
        'content' => function($data){
            $cust = Customer::find()->where(['contract_id' => $data->id])->one();
            $custName = $cust->short_name;
            $inn = $cust->inn;
            if (isset($custName)){
                return "$custName <br> ( ИНН-$inn )";
            }else{
                return 'не задано';
            }
            },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_status',
        'content' => function($data){

            $status = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 'name');
            $date = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 'date');
            $customer = Customer::find()->where(['contract_id' => $data->id])->one();


           if (isset($status)){
               if ($status == 'Заключен'){
                   return  "<p style='background-color:#857da8;color: white;margin: -2px -9px 0px -8px;'>$status от  <br>   $date </p>  ";
               }
               elseif ($status == 'Ждем контракт'){
                   $date  = new DateTime( $date );
                   $date = $date->Format( 'Y-m-d');
                   return  "<p style='background-color:#c77d95;color: white;margin: -2px -9px 0px -8px;'>$status до  <br>   $date </p>  ";
               }
               elseif ($status == 'Подача заявок'){
                   return  "<p style='margin: -2px -9px 0px -8px;'>$status <br>   $customer->deadline </p>  ";
               }
               elseif ($status == 'Проиграли'){
                   return  "<p style='margin: -2px -9px 0px -8px;'>$status</p>  ";
               }
               elseif ($status == 'Ждем торгов'){

                   return  "<p style='margin: -2px -9px 0px -8px;'>$status до  <br>  $customer->auction_date </p>  ";
               }
               elseif ($status == 'Контракт размещен'){
                   $signature_date = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 'signature_deadline');
                   return  "<p style='margin: -2px -9px 0px -8px;'>$status</p><span style='color: red;margin: -2px -9px 0px -8px;'>   Мы подписать до: $signature_date</span>  ";
               }
               elseif ($status == 'Подписали'){
                   $customer_sign_deadline = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 'customer_sign_deadline');
                   return  "<div style='background-color:#83bf80'><p style='color: white'>$status от: $date</p><span style='color: red;'>Заказчик подписать до: $customer_sign_deadline</span> </div>";
               }
               elseif ($status == 'Протокол разногласий'){
                   $re_publish_date = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 're_publish_date');
                   return  "<div><p>$status</p><span style='color: red;'>Ждём контракт до: $re_publish_date</span> </div>";
               }
               elseif ($status == 'Контракт размещен повторно'){
                   $re_publish_date = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $data->id])->one(), 're_publish_date');
                   return  "<div><p>$status</p><span style='color: red;'>Мы подпсисать до: $re_publish_date</span> </div>";
               }
               elseif ($status == 'Не задано'){
                   return 'Не задано';
               }
               else{
                   return "$status от  <br>   $date";
               }
           }else{
               return 'не задано';
           }

        },
        'filter' =>  ArrayHelper::map(OrderStatus::find()->all(), 'name', 'name'),
        'filterType' => 'dropdown',
        'format' => 'raw',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'deliveryStatus',
        'content' => function($data){
            $cargo_status = CargoStatus::find()->where(['contract_id' => $data->id])->one();
            $status = $cargo_status->name;
            $date =  $cargo_status->date;
            $order_status = OrderStatus::find()->where(['contract_id' => $data->id])->one();

            $customer = Customer::find()->where(['contract_id' => $data->id])->one();


            if(isset($status)){
                if ($status == 'Получен заказчиком'){
                    if ($customer->pay_type == 1) {
                        $pay_deadline = new DateTime("$cargo_status->date");
                        $pay_deadline->modify("+$customer->pay_days day");
                        $pay_deadline = $pay_deadline->format('d-m-Y');
                        return  "<div style='background-color:#857da8'><p style='color: white;'>$status от  <br>   $date </p></div> ";
                    } elseif ($customer->pay_type == 3) {
                        $pay_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($cargo_status->date, $customer->pay_days)));
                        return  "<div style='background-color:#857da8'><p style='color: white;'>$status от  <br>   $date </p></div> ";
                    } elseif ($customer->pay_type == 2) {
                        $pay_deadline = $customer->pay_date;
                        return  "<div style='background-color:#857da8'><p style='color: white;'>$status от  <br>   $date </p></div> ";
                    }else{
                        $pay_deadline = 'Необходимо выбрать тип срока оплаты';
                        return  "<div style='background-color:#857da8'><p style='color: white;'>$status от  <br>   $date </p></div> ";
                    }
                }
                elseif ($status == 'Отправлен'){
                    if ($order_status->name == 'Подписали'){
                        if ($customer->date_type == 1) {
                            $delivery_deadline = new DateTime("$order_status->date");
                            $delivery_deadline->modify("+$customer->delivery_days day");
                            $delivery_deadline = $delivery_deadline->format('d-m-Y');
                            return "<div style='background-color:#5591f2'><p style='color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 3) {
                            $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($order_status->date, $customer->delivery_days)));
                            return "<div style='background-color:#5591f2'><p style='color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 2) {
                            $delivery_deadline = $customer->delivery_date;
                            return "<div style='background-color:#5591f2'><p style='color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } else {
                            $delivery_deadline = 'Необходимо выбрать тип срока поставки';
                            return "<div style='background-color:#5591f2'><p style='color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        }
                    }else{
                        return  "<div style='background-color:#5591f2;'><p style='color: white;margin: -2px -9px 0px -8px;'>$status </p> </div> ";
                    }
                }
                elseif ($status == 'Форс мажор'){
                    if ($order_status->name == 'Подписали'){
                        if ($customer->date_type == 1) {
                            $delivery_deadline = new DateTime("$order_status->date");
                            $delivery_deadline->modify("+$customer->delivery_days day");
                            $delivery_deadline = $delivery_deadline->format('d-m-Y');
                            return "<div><p style='background-color:#eb2d2d;color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 3) {
                            $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($order_status->date, $customer->delivery_days)));
                            return "<div><p style='background-color:#eb2d2d;color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 2) {
                            $delivery_deadline = $customer->delivery_date;
                            return "<div><p style='background-color:#eb2d2d;color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } else {
                            $delivery_deadline = 'Необходимо выбрать тип срока поставки';
                            return "<div><p style='background-color:#eb2d2d;color: white;'>$status</p> <span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        }
                    }else{
                        return  "<div><p style='background-color:#eb2d2d;color: white;margin: -2px -9px 0px -8px;'>$status </p> </div> ";
                    }
                }else{
                    if ($order_status->name == 'Подписали'){
                        if ($customer->date_type == 1) {
                            $delivery_deadline = new DateTime("$order_status->date");
                            $delivery_deadline->modify("+$customer->delivery_days day");
                            $delivery_deadline = $delivery_deadline->format('d-m-Y');
                            return "<div><p>$status</p><span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 3) {
                            $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($order_status->date, $customer->delivery_days)));
                            return "<div><p>$status</p><span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } elseif ($customer->date_type == 2) {
                            $delivery_deadline = $customer->delivery_date;
                            return "<div><p>$status</p><span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        } else {
                            $delivery_deadline = 'Необходимо выбрать тип срока поставки';
                            return "<div><p>$status</p><span style='color: red'>срок поставки до $delivery_deadline</span></div>";
                        }
                    }else{
                        $orderStatus = OrderStatus::find()->where(['contract_id' => $data->id])->one();
                        if($orderStatus){
                            if ($orderStatus->name == 'Подписали') {
                                if ($customer->date_type == 1) {
                                    $delivery_deadline = new DateTime("$orderStatus->date");
                                    $delivery_deadline->modify("+$customer->delivery_days day");
                                    $delivery_deadline = $delivery_deadline->format('d-m-Y');
                                } elseif ($customer->date_type == 3) {
                                    $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($orderStatus->date, $customer->delivery_days)));
                                } elseif ($customer->date_type == 2) {
                                    $delivery_deadline = $customer->delivery_date;
                                } else {
                                    $delivery_deadline = 'Необходимо выбрать тип срока поставки';
                                }
                            }else{
//    $delivery_deadline = 'Необходимо установить статус заказаза "Заключен"';
                                if($orderStatus->name == 'Заключен'){
                                    $delivery_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($orderStatus->date, $customer->delivery_days)));
                                } else {
                                    $delivery_deadline = 'Необходимо установить статус заказаза "Заключен"';
                                }
                            }
                        }

                        return "$status<br>$delivery_deadline";
                    }

                }
            }else{
//                return 'не задано';


                return 'Собирается<br>';
            }
        },
        'filter' =>  ArrayHelper::map(CargoStatus::find()->all(), 'name', 'name'),
        'filterType' => 'dropdown',
        'format' => 'raw',

    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'content' => function($data){
            $status = ArrayHelper::getValue(Bidding::find()->where(['contract_id' => $data->id])->one(), 'bid_sum');
            return $status;
        },
    ],
//    [
//        'attribute'=>'for_small_business',
//        'label' => 'Для субъекта малого предпринимательства',
//        'filterType' => 'dropdown',
//        'filter'=>array(
//            1 => 'да',
//            0 => 'нет',
//        ),
//        'filterWidgetOptions' => [
//            'options' => ['prompt' => ''],
//            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
//        ],
//        'content' => function($model){
//            $icon = '';
//            if($model->for_small_business == 0){
//                $icon = 'нет';
//            } else if($model->for_small_business == 1) {
//                $icon = 'да';
//            }
//
//            return $icon;
//        },
//        'width' => '1%',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{parse} {view} {update}',
        'buttons' => [
            'parse' => function ($url,$model) {
                if($model->parsed != 1){
                    return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', ['/contract/parse','id' => $model->id], ['title' => 'Загрузить', 'data-method' => 'post','data-confirm' => "Загрузить данные?"]);
                }
            },
            'delete' => function ($url,$model) {
                $status = ArrayHelper::getValue(OrderStatus::find()->where(['contract_id' => $model->id])->one(), 'name');
                if ($status == 'Проиграли'){
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/contract/delete','id' => $model->id], ['title' => 'Удалить', 'data-method' => 'post','data-confirm' => "Вы действительно хотите удалть этот элемент?"]);
               }
            },
        ],
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];   