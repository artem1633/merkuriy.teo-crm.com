<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Commission */

?>
<div class="commission-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
