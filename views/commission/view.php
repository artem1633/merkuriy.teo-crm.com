<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Commission */
?>
<div class="commission-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'contract_nmck',
            'win_commission',
            'oz_commission',
            'oik_commission',
            'ogo_commission',
//            'contract_id',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
