<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Commission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['commission/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'contract_nmck')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'win_commission')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'oz_commission')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'oik_commission')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'ogo_commission')->textInput() ?>
        </div>
    </div>




    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
    
</div>
