<?php

use app\models\Bidding;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StartMaxPrice */
/* @var $form yii\widgets\ActiveForm */
$contract_id = $model->contract_id;
if($model->isNewRecord == false) {
    $bidding = Bidding::find()->where(['contract_id' => $model->contract_id])->one();
    $model->bidSum = $bidding->bid_sum;
    $model->productCoast = $bidding->product_cost;
    $model->profit = $bidding->profit;
    $model->profitPercent = $bidding->profit_percent;
    $model->decline = $bidding->decline;

}
$onChangeOik = <<<JS
        let checkVal = $(this).find('input:checked').val();
        if (checkVal == 0){
           
            document.getElementById("oik_type_contract").hidden=false; 
            document.getElementById("oik_type_mck").hidden=true; 
        }
       if (checkVal == 1){
            document.getElementById("oik_type_contract").hidden=true; 
            document.getElementById("oik_type_mck").hidden=false;  
        }
JS;
$onChangeOgo = <<<JS
        let checkVal = $(this).find('input:checked').val();
        if (checkVal == 0){
           
            document.getElementById("ogo_type_contract").hidden=false; 
            document.getElementById("ogo_type_mck").hidden=true; 
        }
       if (checkVal == 1){
            
            document.getElementById("ogo_type_contract").hidden=true; 
            document.getElementById("ogo_type_mck").hidden=false;  
        }
JS;
$ozPercent = <<<JS
        let percentVal = $(this).val();
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let ozVal = nmckVal/100*percentVal;
        document.getElementById("startmaxprice-oz").value = ozVal;
JS;
$ozNmck = <<<JS
        let ozNmckVal = $(this).val();
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let percentVal = ozNmckVal*100/nmckVal;
        document.getElementById("startmaxprice-oz_percent").value = percentVal.toFixed(2);
JS;
$oikContract = <<<JS
        let oikVal = $(this).val();
        let bidVal = document.getElementById("startmaxprice-bidsum").value;
        let percentVal = oikVal*100/bidVal;
        document.getElementById("startmaxprice-oik_percent").value = percentVal.toFixed(2);
JS;
$oikNmck = <<<JS
        let oikVal = $(this).val();
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let percentVal = oikVal*100/nmckVal;
        document.getElementById("startmaxprice-oik_percent").value = percentVal.toFixed(2);
JS;
$oikPercent = <<<JS
        let oikPercentVal = $(this).val();
        let bidVal = document.getElementById("startmaxprice-bidsum").value;
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let sumBid = bidVal/100*oikPercentVal;
        let sumNmck = nmckVal/100*oikPercentVal;
        
        document.getElementById("startmaxprice-oik_contract").value = sumBid;
        document.getElementById("startmaxprice-oik_mck").value = sumNmck;
JS;
$ogoContract = <<<JS
       let ogoVal = $(this).val();
        let bidVal = document.getElementById("startmaxprice-bidsum").value;
        let percentVal = ogoVal*100/bidVal;
        document.getElementById("startmaxprice-ogo_percent").value = percentVal.toFixed(2);
JS;
$ogoNmck = <<<JS
        let ogoVal = $(this).val();
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let percentVal = ogoVal*100/nmckVal;
        document.getElementById("startmaxprice-ogo_percent").value = percentVal.toFixed(2);
JS;
$ogoPercent = <<<JS
        let ogoPercentVal = $(this).val();
        let bidVal = document.getElementById("startmaxprice-bidsum").value;
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let sumBid = bidVal/100*ogoPercentVal;
        let sumNmck = nmckVal/100*ogoPercentVal;
        
        document.getElementById("startmaxprice-ogo_contract").value = sumBid;
        document.getElementById("startmaxprice-ogo_mck").value = sumNmck;
JS;
$bidtype = <<<JS
        let bidVal = $(this).val();
        let nmckVal = document.getElementById("startmaxprice-contract_start_max_price").value;
        let perc = 100 - ((bidVal/nmckVal)*100);
        if (perc < 25){
            document.getElementById("decline").hidden=true;
                let oikPercentVal = document.getElementById("startmaxprice-oik_percent").value;
                let ogoPercentVal = document.getElementById("startmaxprice-ogo_percent").value;
                let oikVal = bidVal/100*oikPercentVal;
                let ogoVal = bidVal/100*ogoPercentVal;
                document.getElementById("startmaxprice-ogo_contract").value = ogoVal;
                document.getElementById("startmaxprice-oik_contract").value = oikVal;
        }
        if (perc > 25){
            document.getElementById("decline").hidden=false; 
            let oikPercentVal = document.getElementById("startmaxprice-oik_percent").value;
            let ogoPercentVal = document.getElementById("startmaxprice-ogo_percent").value;
            let oikVal = bidVal/100*oikPercentVal;
            let ogoVal = bidVal/100*ogoPercentVal;
            oikVal = oikVal + (oikVal/2);
            ogoVal = ogoVal + ((ogoVal/2));
            
            document.getElementById("startmaxprice-ogo_contract").value = ogoVal;
            document.getElementById("startmaxprice-oik_contract").value = oikVal;
            
        }
JS;
?>

<div class="start-max-price-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['start-max-price/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Начальная и максимальная цена контракта</h4>
                </div>
                <div class="panel-body" style="height: 350px; overflow-y: auto;">
                    <div class="row"style="margin-top: 5px">
                        <div class="col-md-3">
                            <?= $form->field($model, 'contract_start_max_price')->textInput()->label('НМЦК') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'oz')->textInput([
                                'onChange' => $ozNmck
                            ])->label('ОЗ') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'oz_percent')->textInput([
                                'onChange' => $ozPercent
                            ])->label('ОЗ %') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'oz_bg')->checkbox(['style' => 'margin-top: 30px;']) ?>
                        </div>

                    </div>
                    <div class="row"style="margin-top: 5px">
                        <div class="col-md-3">
                            <?= $form->field($model, 'oik_type')->radioList([
                                0 =>'от Контратка',
                                1 => 'от НМЦК'
                            ],['id' => 'oik_type','onchange' => $onChangeOik]

                            )?>
                        </div>
                        <div class="col-md-3"id="oik_type_contract" <?php if ($model->oik_type == 1 ) echo 'hidden'?>>
                            <?= $form->field($model, 'oik_contract')->textInput([
                                'onchange' => $oikContract
                            ]) ?>
                        </div>
                        <div class="col-md-3"id="oik_type_mck" <?php if ($model->oik_type == 0) echo 'hidden'?>>
                            <?= $form->field($model, 'oik_mck')->textInput([
                                'onchange' => $oikNmck
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'oik_percent')->textInput([
                                'onchange' => $oikPercent
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'oik_bg')->checkbox(['style' => 'margin-top: 30px;']) ?>
                        </div>
                    </div>
                    <div class="row"style="margin-top: 5px">
                        <div class="col-md-3">
                            <?= $form->field($model, 'ogo_type')->radioList([
                                0 =>'от Контратка',
                                1 => 'от НМЦК'
                            ],['id' => 'oik_type','onchange' => $onChangeOgo]

                            )?>
                        </div>
                        <div class="col-md-3"id="ogo_type_contract" <?php if ($model->ogo_type == 1 ) echo 'hidden'?>>
                            <?= $form->field($model, 'ogo_contract')->textInput([
                                 'onchange' => $ogoContract
                            ]) ?>
                        </div>
                        <div class="col-md-3"id="ogo_type_mck" <?php if ($model->ogo_type == 0) echo 'hidden'?>>
                            <?= $form->field($model, 'ogo_mck')->textInput([
                                'onchange' => $ogoNmck
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'ogo_percent')->textInput([
                                'onchange' => $ogoPercent
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'ogo_bg')->checkbox(['style' => 'margin-top: 30px;']) ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Сумма контракта</h4>
                </div>
                <div class="panel-body" style="height:170px; overflow-y: visible;">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'bidSum')->textInput([
                                'onchange' => $bidtype
                            ]) ?>
                        </div>
                        <div class="col-md-6"id="decline"<?php if ($model->decline == 0) echo 'hidden'?>>
                            <div class="dec-wrap"style="background-color: lightcoral;border-radius:15px;">
                                <h4 style="margin: 5px;">Снижение > 25%</h4>
                                <p style="margin: 5px 0px 5px 10px;color: white;">
                                    Думай всегда
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'productCoast')->textInput() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'profit')->textInput() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'profitPercent')->textInput() ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Дополнительные расходы</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['costs/create', 'contract_id' => $contract_id, 'containerPjaxReload' => $containerPjaxReload],
                            ['role'=>'modal-remote','title'=> 'Добавить Дополнительные расходы','class'=>'btn btn-warning btn-xs'])?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style=" overflow-y: auto;">
                    <?= $this->render('@app/views/costs/index',[
                        'searchModel' => $costsSearchModel,
                        'dataProvider' => $costsDataProvider,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


