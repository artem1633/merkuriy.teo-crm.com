<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StartMaxPrice */
?>
<div class="start-max-price-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
