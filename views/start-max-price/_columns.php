<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_start_max_price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'oz',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'oz_percent',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'oik_mck',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'oik_contract',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'oik_percent',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'ogo_mck',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'ogo_contract',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'ogo_percent',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'contract_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Скачать',
        'content' => function($model){
            return Html::a('<i class="fa fa-pencil text-success" style="font-size: 16px;"></i>', ['start-max-price/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote'])
                .' '.Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['start-max-price/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить файл'
                ]);
        }
    ],

];   