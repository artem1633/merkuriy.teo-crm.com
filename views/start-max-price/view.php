<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StartMaxPrice */
?>
<div class="start-max-price-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'contract_start_max_price',
            'oz',
            'oz_percent',
            'oik_mck',
            'oik_contract',
            'oik_percent',
            'ogo_mck',
            'ogo_contract',
            'ogo_percent',
//            'contract_id',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
