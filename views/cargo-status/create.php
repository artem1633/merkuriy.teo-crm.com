<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CargoStatus */

?>
<div class="cargo-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
