<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CargoStatus */
/* @var $form yii\widgets\ActiveForm */
if($model->date == null){
    $model->date = date('Y-m-d');
}
?>

<div class="cargo-status-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['cargo-status/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->dropDownList([
//                'Не задано' => 'Не задано',
                'Собирается' => 'Собирается',
                'Готов к отправке' => 'Готов к отправке',
                'Отправлен' => 'Отправлен',
                'Получен заказчиком' => 'Получен заказчиком',
                'Форс мажор' => 'Форс мажор',
            ],[
                'id'=>'cargo_type_name', 'onChange'=>'$(document).ready(function() {
                       if(document.getElementById("cargo_type_name").value == "Получен заказчиком" ){
                            document.getElementById("cargo_type_name_in").hidden=false;
                        }else{
                         document.getElementById("cargo_type_name_in").hidden=true;
                        }
                       
                    });'
            ]) ?>
        </div>
        <div class="col-md-6" id="cargo_type_name_in" <?php if ($model->name == 'Не задано' or $model->name == 'Собирается' or $model->name === null or $model->name == 'Готов к отправке' or $model->name == 'Отправлен' or $model->name == 'Форс мажор') echo 'hidden'?>>
            <?= $form->field($model,'date')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату и время ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )?>
        </div>
    </div>






    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
