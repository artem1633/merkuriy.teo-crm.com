<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CargoStatus */
?>
<div class="cargo-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'date',
            'contract_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
