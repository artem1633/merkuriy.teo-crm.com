<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['payments/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'delivery_days')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'delivery_date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pay_days')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'pay_date')->input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'day_type')->dropDownList([
                'календарных'  => 'календарных',
                'рабоичх' => 'рабоичх',
            ]) ?>
        </div>
    </div>




    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
    
</div>
