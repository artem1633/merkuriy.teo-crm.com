<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
?>
<div class="payments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
