<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
?>
<div class="payments-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'delivery_days',
            'pay_days',
            'delivery_date',
            'pay_date',
            'day_type',
//            'contract_id',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
