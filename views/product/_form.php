<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
$getSum = <<<JS
        let productCount = $(this).val();
        let productPrice = document.getElementById("product-price").value;
        let sum = productCount * productPrice;
        
        document.getElementById("product-sum").value = sum;
      
JS;
$gettSum = <<<JS
        let productPrice = $(this).val();
        let productCount = document.getElementById("product-count").value;
        let sum = productCount * productPrice;
        
        document.getElementById("product-sum").value = sum;
JS;
$getCount = <<<JS
        let productSum = $(this).val();
         let productCount = document.getElementById("product-count").value;
        let count = productSum / productCount;
        
        document.getElementById("product-price").value = count;
JS;
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'count')->textInput([
                'onchange' => $getSum
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'price')->textInput([
                'onchange' => $gettSum
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'sum')->textInput([
                'onchange' => $getCount
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'rise')->checkbox([
                'uncheck'=>0,
                'value' => 1
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'rise_sum')->textInput() ?>
        </div>
    </div>


    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>




  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
