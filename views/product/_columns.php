<?php

use app\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;

return [

//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'vendor_code',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'balance_ignore',
//        'content' => function($model){
//            $icon = '';
//            if($model->balance_ignore == 0){
//                $icon = 'нет';
//            } else if($model->balance_ignore == 1) {
//                $icon = 'да';
//            }
//
//            return $icon;
//        },
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data){
            return Html::a("$data->name", ['product/update', 'id' => $data->id, 'containerPjaxReload' => '#pjax-container-info-container-2'], ['role' => 'modal-remote']);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'pageSummary' => true,
        'pageSummaryOptions' =>[
            'prepend' => 'Итого: '
        ],
        'content' => function($model){
            return "{$model->price}";
//            return "{$model->price} (".round($model->price / 72.4, 2)."%)";
        }
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'sum',
         'pageSummary' => true,
         'pageSummaryOptions' =>[
             'prepend' => 'Итого: '
         ],
         'content' => function($model){
             return "{$model->sum}";
//             return "{$model->sum} (".round($model->sum / 1820, 2)."%)";
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'balance',
         'content' => function($model){
            $riseSum = $model->rise_sum;

             if ($model->rise == 1){
                 $icon = Html::tag('p', '<i class="fa fa-check text-success" style="font-size: 16px;"></i>  ' . $riseSum);
             }else{
                 $icon = Html::tag('p', '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>');
             }
             return $icon;
         },
     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'rise_sum',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'availability',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'buy_place',
//     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'balance',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment',
     ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'contract_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Действия',
        'content' => function($model){
            return Html::a('<i class="fa fa-pencil text-success" style="font-size: 16px;"></i>', ['product/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container-2'], ['role' => 'modal-remote'])
                .' '.Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['product/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container-2'],[
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить файл'
                ]);
        }
    ],

];   