<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
?>
<div class="product-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vendor_code',
            'balance_ignore',
            'name',
            'count',
            'price',
            'sum',
            'rise',
            'rise_sum',
            'availability',
            'buy_place',
            'comment:ntext',
            'balance',
            'contract_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
