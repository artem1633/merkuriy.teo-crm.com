<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price_all',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'commission_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'costs_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'percent',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'profit',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'balance_profit',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'contract_id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Скачать',
        'content' => function($model){
            return Html::a('<i class="fa fa-pencil text-success" style="font-size: 16px;"></i>', ['calculation/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote'])
                .' '.Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['calculation/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить файл'
                ]);
        }
    ],
//

];   