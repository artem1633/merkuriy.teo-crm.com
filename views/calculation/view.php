<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
?>
<div class="calculation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'price_all',
            'commission_sum',
            'costs_sum',
            'percent',
            'profit',
            'balance_profit',
//            'contract_id',
        ],
    ]) ?>

</div>
