<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Calculation */

?>
<div class="calculation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
