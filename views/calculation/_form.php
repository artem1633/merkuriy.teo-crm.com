<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calculation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calculation-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['calculation/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'price_all')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'commission_sum')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'costs_sum')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'percent')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'profit')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'balance_profit')->textInput() ?>
        </div>
    </div>



    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
