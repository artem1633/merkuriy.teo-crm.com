<?php

use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['customer/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ])?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'purchase_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'date_type')->
            dropDownList([
                    '8' => 'не задано',
                    '3' => 'Рабочих дней',
                    '1' => 'Календарных дней',
                    '2' => 'Выбор даты',
               ],
                ['id'=>'date_type', 'onChange'=>'$(document).ready(function() {
                        if ( document.getElementById("date_type").value == "3") {
                              document.getElementById("date_in").hidden=false; 
                            document.getElementById("date_out").hidden=true; 
                        }
                        if ( document.getElementById("date_type").value == "1") {
                            document.getElementById("date_in").hidden=false; 
                            document.getElementById("date_out").hidden=true;  
                        }
                        if ( document.getElementById("date_type").value == "2") {
                            document.getElementById("date_in").hidden=true; 
                            document.getElementById("date_out").hidden=false; 
                        }
                         if ( document.getElementById("date_type").value == "8") {
                            document.getElementById("date_in").hidden=true; 
                            document.getElementById("date_out").hidden=true; 
                        }     
                    });']) ?>
        </div>

        <div class="col-md-1"id="date_in" <?php if ($model->date_type == 2 or $model->date_type == null or $model->date_type == 8) echo 'hidden'?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model,'delivery_days')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="col-md-2"id="date_out" <?php if ($model->date_type == 3 or $model->date_type == 1 or $model->date_type == null or $model->date_type == 8) echo 'hidden'?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model,'delivery_date')->widget(
                        DatePicker::className(),[
                            'options' => ['placeholder' => 'Выберите дату ...'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]
                    ) ?>
                </div>
            </div>
        </div>



        <div class="col-md-2">
            <?= $form->field($model, 'pay_type')->dropDownList([
                '8' => 'не задано',
                '3' => 'Рабочих дней',
                '1' => 'Календарных дней',
                '2' => 'Выбор даты',
            ],['id'=>'pay_type', 'onChange'=>'$(document).ready(function() {
                        if ( document.getElementById("pay_type").value == "3") {
                            document.getElementById("pay_in").hidden=false; 
                            document.getElementById("pay_out").hidden=true; 
                        }
                        if ( document.getElementById("pay_type").value == "1") {
                            document.getElementById("pay_in").hidden=false; 
                            document.getElementById("pay_out").hidden=true;  
                        }
                        if ( document.getElementById("pay_type").value == "2") {
                            document.getElementById("pay_in").hidden=true; 
                            document.getElementById("pay_out").hidden=false; 
                        } 
                         if ( document.getElementById("pay_type").value === "8") {
                            document.getElementById("pay_in").hidden=true; 
                            document.getElementById("pay_out").hidden=true; 
                        }  
                    });']) ?>
        </div>

        <div class="col-md-1"id="pay_in" <?php if ($model->pay_type == 2 or $model->pay_type == null or $model->pay_type == 8) echo 'hidden'?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model,'pay_days')->textInput()?>
                </div>
            </div>
        </div>
        <div class="col-md-2"id="pay_out" <?php if ($model->pay_type == 3 or $model->pay_type == 1 or $model->pay_type == null or $model->pay_type == 8) echo 'hidden'?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model,'pay_date')->widget(
                        DatePicker::className(),[
                            'options' => ['placeholder' => 'Выберите дату ...'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]
                    )?>
                </div>
            </div>
        </div>



    </div>




    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'shipping_address')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'lot_link')->textInput(['maxlength' => true]) ?>
        </div>


    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'lot_link_playground')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'lot_link_portal')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'conducting_way')->widget(Select2::classname(), [
                'data' => [
                    'ЕП закупка единственного поставщка'  => 'ЕП закупка единственного поставщка',
                    'К конкурс' => 'К конкурс',
                    'А аукцион' => 'А аукцион',
                    'ЗП запрос предложений' => 'ЗП запрос предложений',
                    'ЗЦ запрос цен' => 'ЗЦ запрос цен',
                    'ЗК запрос котировок' => 'ЗК запрос котировок',
                    'ИС иной способ' => 'ИС иной способ',
                ],
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите способ ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model,'deadline')->widget(
                    DateTimePicker::className(),[
                            'options' => ['placeholder' => 'Выберите дату и время ...'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd hh:ii',
                                'todayHighlight' => true
                            ]
                        ]
            )
           ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'bid_consideration')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            ) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model,'auction_date')->widget(
                DateTimePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату и время ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd hh:ii',
                        'todayHighlight' => true
                    ]
                ]
            )
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'link_1s')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'for_small_business')->radioList([
                0 =>'СМП',
                1 => 'без СМП'
            ] )?>
        </div>
    </div>






  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>
