<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
?>
<div class="customer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'inn',
            'kpp',
            'full_name',
            'short_name',
            'shipping_address',
            'delivery_date',
            'pay_date',
            'lot_link',
            'lot_link_playground',
            'lot_link_portal',
            'conducting_way',
            'deadline',
            'bid_consideration',
            'auction_date',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
