<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'inn',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'kpp',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'full_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'short_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'shipping_address',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'delivery_date',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'pay_date',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'lot_link',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'lot_link_playground',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'lot_link_portal',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'conducting_way',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'deadline',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'bid_consideration',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'auction_date',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Скачать',
        'content' => function($model){
            return Html::a('<i class="fa fa-pencil text-success" style="font-size: 16px;"></i>', ['customer/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote'])
                .' '.Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['customer/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить файл'
                ]);
        }
    ],

];   