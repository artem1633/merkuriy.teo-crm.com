<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CargoShip */
?>
<div class="cargo-ship-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'shipper_id',
            'track',
            'date',
            'contract_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
