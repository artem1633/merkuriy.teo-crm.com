<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CargoShip */
?>
<div class="cargo-ship-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
