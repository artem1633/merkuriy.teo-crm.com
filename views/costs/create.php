<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Costs */

?>
<div class="costs-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
