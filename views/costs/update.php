<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Costs */
?>
<div class="costs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
