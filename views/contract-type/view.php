<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ContractType */
?>
<div class="contract-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
