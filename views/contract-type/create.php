<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContractType */

?>
<div class="contract-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
