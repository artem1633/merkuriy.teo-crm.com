<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
?>
<div class="order-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'date',
//            'contract_id',
            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
