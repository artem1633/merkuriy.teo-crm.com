<?php

use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
/* @var $form yii\widgets\ActiveForm */

if($model->date == null){
    $model->date = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($customer->auction_date, 5)));
}

if($model->signature_deadline == null && $model->name == 'Ждем контракт'){
    $model->signature_deadline = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($model->date, 5)));
}

if(($model->customer_sign_deadline == null && $model->name == 'Контракт размещен') || $model->name == 'Контракт размещен повторно'){
    if($model->fail_protocol != null){
        $model->customer_sign_deadline = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($model->re_publish_date, 3)));
    } else {
        $model->customer_sign_deadline = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($model->signature_deadline, 3)));

    }
}

if($model->fail_protocol == null && $model->name == 'Контракт размещен'){
    $model->fail_protocol = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($model->customer_sign_deadline, 3)));
}

if($model->re_publish_date == null && $model->name == 'Протокол разногласий'){
    $model->re_publish_date = date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($model->fail_protocol, 3)));
}

?>

<div class="order-status-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['order-status/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->dropDownList([
                    'Подача заявок' => 'Подача заявок',
                    'Ждем торгов' => 'Ждем торгов',
                    'Ждем контракт' => 'Ждем контракт',
                    'Контракт размещен' => 'Контракт размещен',
                    'Протокол разногласий' => 'Протокол разногласий',
                    'Контракт размещен повторно' => 'Контракт размещен повторно',
                    'Подписали' => 'Подписали',
                    'Заключен' => 'Заключен',
                    'Проиграли' => 'Проиграли'
            ],[
                'id'=>'date_type_name', 'onChange'=>'$(document).ready(function() {
//                        if ( document.getElementById("date_type_name").value == "Подача заявок" || document.getElementById("date_type_name").value == "Ждем торгов") {
                        if ( document.getElementById("date_type_name").value == "Ждем торгов") {
                              document.getElementById("date_type_name_in").hidden=true;  
                              document.getElementById("date_signature_in").hidden=true; 
                              document.getElementById("customer_signature_in").hidden=true;
                              document.getElementById("is_fair").hidden=true;
                               document.getElementById("is_fair").value = 0;
                        }else{
                            if(document.getElementById("date_type_name").value != "Подача заявок" && document.getElementById("date_type_name").value != "Ждем торгов"){
                                document.getElementById("date_type_name_in").hidden=false;                            
                                document.getElementById("date_signature_in").hidden=true;
                                document.getElementById("customer_signature_in").hidden=true;  
                                document.getElementById("fail_protocol").hidden=true; 
                                document.getElementById("re_publish_date").hidden=true; 
                                document.getElementById("is_fair").hidden=true; 
                                document.getElementById("is_fair").value = 0;
                            }
                        }
                        if(document.getElementById("date_type_name").value == "Контракт размещен" ){
                            document.getElementById("date_signature_in").hidden=false;
                            document.getElementById("customer_signature_in").hidden=true; 
                            document.getElementById("is_fair").hidden=true; 
                            document.getElementById("is_fair").value = 0;
                        }
                        if(document.getElementById("date_type_name").value == "Подписали" ){
                            document.getElementById("customer_signature_in").hidden=false; 
                            document.getElementById("is_fair").hidden=false; 
                        }
                        if(document.getElementById("date_type_name").value == "Проиграли" ){
                            document.getElementById("date_type_name_in").hidden=true; 
                            document.getElementById("is_fair").hidden=true; 
                            document.getElementById("is_fair").value = 0;
                        }
                        if(document.getElementById("date_type_name").value == "Не задано" ){
                            document.getElementById("date_type_name_in").hidden=true; 
                            document.getElementById("is_fair").hidden=true; 
                            document.getElementById("is_fair").value = 0;
                        }
                        if(document.getElementById("date_type_name").value == "Протокол разногласий" ){
                            document.getElementById("fail_protocol").hidden=false; 
                            document.getElementById("is_fair").hidden=true; 
                            document.getElementById("is_fair").value = 0;
                        }
                        if(document.getElementById("date_type_name").value == "Контракт размещен повторно" ){
                            document.getElementById("re_publish_date").hidden=false;
                            document.getElementById("is_fair").hidden=true;  
                            document.getElementById("is_fair").value = 0;
                        }
                    });'
            ]) ?>
        </div>
        <div class="col-md-6" id="date_signature_in" <?php if ($model->name !== 'Контракт размещен') echo 'hidden'?>>
            <?= $form->field($model,'signature_deadline')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )?>
        </div>
        <div class="col-md-6" id="customer_signature_in" <?php if ($model->name !== 'Подписали') echo 'hidden'?>>
            <?= $form->field($model,'customer_sign_deadline')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )?>
        </div>
        <div class="col-md-6" id="fail_protocol" <?php if ($model->name !== 'Протокол разногласий') echo 'hidden'?>>
            <?= $form->field($model,'fail_protocol')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )?>
        </div>
        <div class="col-md-6" id="re_publish_date" <?php if ($model->name !== 'Контракт размещен повторно') echo 'hidden'?>>
            <?= $form->field($model,'re_publish_date')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )?>
        </div>
        <div class="col-md-6" id="is_fair"<?php if ($model->name === null or $model->name !== 'Подписали') echo 'hidden'?>>
            <?= $form->field($model, 'is_fair')->checkbox([
                'uncheck'=>0,
                'value' => 1
            ]) ?>
        </div>
        <div class="col-md-6" id="date_type_name_in" <?php if ($model->name == 'Подача заявок' or $model->name == 'Ждем торгов' or $model->name === null or $model->name == 'Проиграли') echo 'hidden'?>>
            <?php
            echo $form->field($model,'date')->widget(
                DatePicker::className(),[
                    'options' => ['placeholder' => 'Выберите дату  ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )
            ?>
        </div>
    </div>






    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS

var fail = false;

$('#date_type_name_in input').change(function(){
    var date = $(this).val();

    $.get('/contract/calc-date?date='+date+'&value=5', function(response){
        $('#date_signature_in input').val(response.date);        
    });
});

$('#date_signature_in input').change(function(){
    var date = $(this).val();

    $.get('/contract/calc-date?date='+date+'&value=3', function(response){
        $('#fail_protocol input').val(response.date);        
        $('#fail_protocol input').trigger('change', {unreal: true});
    });
});


$('#fail_protocol input').change(function(e){
    var date = $(this).val();

    $.get('/contract/calc-date?date='+date+'&value=3', function(response){
        $('#re_publish_date input').val(response.date);       
        if(fail){
            $('#re_publish_date input').trigger('change');            
        }
    });
});

$('#re_publish_date input').change(function(){
    var date = $(this).val();

    $.get('/contract/calc-date?date='+date+'&value=3', function(response){
        $('#customer_signature_in input').val(response.date);        
    });
});


JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>