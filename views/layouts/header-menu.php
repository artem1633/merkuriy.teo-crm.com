<?php

use yii\helpers\Url;

?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Контракты', 'icon' => 'fa  fa-th', 'url' => ['/contract'],],
//                    ['label' => 'Кандидаты', 'icon' => 'fa  fa-users', 'url' => ['/candidate'],],

//                    ['label' => 'Проекты', 'icon' => 'fa fa-briefcase', 'url' => ['/contract'],],
//                    ['label' => 'Спринты', 'icon' => 'fa  fa-flag', 'url' => ['/order'],],
//                    ['label' => 'Задачи', 'icon' => 'fa fa-sitemap', 'url' => ['/task'],],
                    ['label' => 'Отчет', 'icon' => 'fa fa-sitemap', 'url' => ['/report'],],
//                    ['label' => 'Очет', 'icon' => 'fa fa-area-chart', 'url' => ['/report'],],

//
                    ['label' => 'Справочники', 'icon' => 'fa fa-book', 'url' => '/user', 'options' => ['class' => 'has-sub'],'visible' =>Yii::$app->user->identity->isSuperAdmin(),
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Типы лотов', 'icon' => 'fa  fa-user-o', 'url' => ['/contract-type']],
//                            ['label' => 'Виды работ', 'icon' => 'fa  fa-user-o', 'url' => ['/work-type']],
//                            ['label' => 'Статус задач', 'icon' => 'fa  fa-user-o', 'url' => ['/task-status']],
                            ['label' => 'Выходные', 'icon' => 'fa  fa-user-o', 'url' => ['/weekend']],
//
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
