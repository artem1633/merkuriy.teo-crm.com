<?php

use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->

                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="navbar-user">
                            <?= Html::a('<i class="fa fa-question-circle" style="font-size: 20px;"></i>', ['ticket/index'], ['title' => 'Тикеты', 'style' => 'cursor: pointer;']) ?>
                        </li>
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/<?= Yii::$app->user->identity->realAvatar ?>" style="object-fit: cover;" data-role="avatar-view" alt="">
                                <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li class="arrow"></li>
                                <li> <?= Html::a('Настройки пользователи', ['user/profile']) ?> </li>
                                <li class="divider"></li>
                                <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>