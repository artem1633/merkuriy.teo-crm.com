<?php

use app\models\Bidding;
use app\models\CargoStatus;
use app\models\ContractType;
use app\models\Customer;
use app\models\OrderStatus;
use app\models\StartMaxPrice;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

return [
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'auction_number',
        'content' => function($data){
//            $status = ArrayHelper::getColumn(ContractType::find()->where(['id' => $data->auction_number])->all(), 'name');
            return \yii\helpers\Html::a($data->auction_number,['contract/view', 'id' => $data->id]);

        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fullName',
        'content' => function($data){
            $cust = Customer::find()->where(['contract_id' => $data->id])->one();
            $custName = $cust->short_name;
            $inn = $cust->inn;
            if (isset($custName)){
                return "$custName <br> ( ИНН-$inn )";
            }else{
                return 'не задано';
            }
            },
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'content' => function($data){
            $output = ArrayHelper::getValue(Bidding::find()->where(['contract_id' => $data->id])->one(), 'bid_sum');

            $output .= Html::checkbox('checkbox-payment', $data->payed, ['onchange' => '
                var id = $(this).data("model");
                var self = this;
                if($(this).is(":checked")){
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed&value=1", function(response){
                    });
                } else {
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed&value=0", function(response){
                    });
                }
            ', 'style' => 'margin-left: 7px;', 'data-model' => $data->id]);

            return $output;
        },
        'width' => '5%',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Срок оплаты до',
        'content' => function($data){

            $cargo_status = CargoStatus::find()->where(['contract_id' => $data->id])->one();
            $status = $cargo_status->name;
            $date =  $cargo_status->date;
            $order_status = OrderStatus::find()->where(['contract_id' => $data->id])->one();

            $customer = Customer::find()->where(['contract_id' => $data->id])->one();
            if(isset($status)){
                if ($status == 'Получен заказчиком'){
                    if ($customer->pay_type == 1) {
                        $pay_deadline = new DateTime("$cargo_status->date");
                        $pay_deadline->modify("+$customer->pay_days day");
                        $pay_deadline = $pay_deadline->format('d-m-Y');
                        return  "$pay_deadline";
                    } elseif ($customer->pay_type == 3) {
                        $pay_deadline = date('d-m-Y', strtotime(\app\helpers\WorkDay::getWorkDay($cargo_status->date, $customer->pay_days)));
                        return  "$pay_deadline";
                    } elseif ($customer->pay_type == 2) {
                        $pay_deadline = $customer->pay_date;
                        return  "$pay_deadline";
                    }else{
                        $pay_deadline = 'Необходимо выбрать тип срока оплаты';
                        return  "$pay_deadline";
                    }
                }
            }else{
                return 'не задано';
            }

        },
        'width' => '5%',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'ОЗ',
        'content' => function($data){
            $startMaxPrice = StartMaxPrice::find()->where(['contract_id' => $data->id])->one();
            return $startMaxPrice->oz;
        },
        'width' => '5%',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'ОИК',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'content' => function($data){
            $output = '';
            $startMaxPrice = StartMaxPrice::find()->where(['contract_id' => $data->id])->one();

            if($startMaxPrice){
                $output = $startMaxPrice->oik_contract;
            }

            if($startMaxPrice->oik_contract == 0){
                $data->payed_oik = 1;
            }

            $output .= Html::checkbox('checkbox-payment', $data->payed_oik, ['onchange' => '
                var id = $(this).data("model");
                var self = this;
                if($(this).is(":checked")){
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed_oik&value=1", function(response){
                    });
                } else {
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed_oik&value=0", function(response){
                    });
                }
            ', 'style' => 'margin-left: 7px;', 'data-model' => $data->id]);

            return $output;
        },
        'width' => '5%',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'ОГО',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'content' => function($data){
            $output = '';
            $startMaxPrice = StartMaxPrice::find()->where(['contract_id' => $data->id])->one();

            if($startMaxPrice){
                $output = $startMaxPrice->ogo_contract;
            }

            if($startMaxPrice->ogo_contract == 0){
                $data->payed_ogo = 1;
            }

            $output .= Html::checkbox('checkbox-payment', $data->payed_ogo, ['onchange' => '
                var id = $(this).data("model");
                var self = this;
                if($(this).is(":checked")){
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed_ogo&value=1", function(response){
                    });
                } else {
                    $.get("/contract/pay-toggle?id="+id+"&attribute=payed_ogo&value=0", function(response){
                    });
                }
            ', 'style' => 'margin-left: 7px;', 'data-model' => $data->id]);

            return $output;
        },
        'width' => '5%',
    ],
//    [
//        'attribute'=>'for_small_business',
//        'label' => 'Для субъекта малого предпринимательства',
//        'filterType' => 'dropdown',
//        'filter'=>array(
//            1 => 'да',
//            0 => 'нет',
//        ),
//        'filterWidgetOptions' => [
//            'options' => ['prompt' => ''],
//            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
//        ],
//        'content' => function($model){
//            $icon = '';
//            if($model->for_small_business == 0){
//                $icon = 'нет';
//            } else if($model->for_small_business == 1) {
//                $icon = 'да';
//            }
//
//            return $icon;
//        },
//        'width' => '1%',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
];   