<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Control */
?>
<div class="control-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'contract_id',
            'sum',
            [
                'attribute' => 'checked',
                'value' => function($model) {
                    if ($model->checked === 1) {
                        return 'да';
                    } elseif ($model->checked === 0) {
                        return 'нет';
                    }
                }
            ],
        ],
    ]) ?>

</div>
