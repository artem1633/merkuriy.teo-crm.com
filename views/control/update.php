<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Control */
?>
<div class="control-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
