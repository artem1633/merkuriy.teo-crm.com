<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Control */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="control-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['control/update','id' => $model->id,'containerPjaxReload' => $containerPjaxReload ],
    ]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'sum')->textInput() ?>
            <?= $form->field($model, 'checked')->checkbox([
                'uncheck'=>0,
                'checked'=>1,
            ],true) ?>
        </div>


    </div>






    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
