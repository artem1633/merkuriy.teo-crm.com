<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Control */

?>
<div class="control-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
