<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerContact */

?>
<div class="customer-contact-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
