<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerContact */
?>
<div class="customer-contact-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'email:email',
            'phone',
            'contract_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
