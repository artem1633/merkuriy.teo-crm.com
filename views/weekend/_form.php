<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Weekend */
/* @var $form yii\widgets\ActiveForm */

$count = [];

$count[1] = cal_days_in_month(CAL_GREGORIAN, 1, 2018);
$count[2] = cal_days_in_month(CAL_GREGORIAN, 2, 2018);
$count[3] = cal_days_in_month(CAL_GREGORIAN, 3, 2018);
$count[4] = cal_days_in_month(CAL_GREGORIAN, 4, 2018);
$count[5] = cal_days_in_month(CAL_GREGORIAN, 5, 2018);
$count[6] = cal_days_in_month(CAL_GREGORIAN, 6, 2018);
$count[7] = cal_days_in_month(CAL_GREGORIAN, 7, 2018);
$count[8] = cal_days_in_month(CAL_GREGORIAN, 8, 2018);
$count[9] = cal_days_in_month(CAL_GREGORIAN, 9, 2018);
$count[10] = cal_days_in_month(CAL_GREGORIAN, 10, 2018);
$count[11] = cal_days_in_month(CAL_GREGORIAN, 11, 2018);
$count[12] = cal_days_in_month(CAL_GREGORIAN, 12, 2018);

$countJs = json_encode($count);


$months = [
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4 => 'Апрель',
    5 => 'Май',
    6 => 'Июнь',
    7 => 'Июль',
    8 => 'Август',
    9 => 'Сентябрь',
    10 => 'Октябрь',
    11 => 'Ноябрь',
    12 => 'Декабрь',
];

$days28 = '';

for($i = 1; $i <= 28; $i++){
    $days28 .= '<option value="'.$i.'">'.$i.'</option>';
}

$days30 = '';

for($i = 1; $i <= 30; $i++){
    $days30 .= '<option value="'.$i.'">'.$i.'</option>';
}

$days31 = '';

for($i = 1; $i <= 31; $i++){
    $days31 .= '<option value="'.$i.'">'.$i.'</option>';
}



?>

<div class="weekend-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'month')->dropDownList($months, ['prompt' => 'Выберите месяц', 'onchange' => 'init(false)']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'day')->dropDownList([], ['prompt' => 'Выберите день']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$modelDayValue = $model->day ? $model->day : 'null';

$script = <<< JS

    var countArr = {$countJs};

    var value = {$modelDayValue};

    function init(start) {
        var month = $('#weekend-month').val();
        if(month !== null){
            if(countArr[month] === 28){
                $('#weekend-day').html('{$days28}');
            }
            if(countArr[month] === 30){
                $('#weekend-day').html('{$days30}');
            }
            if(countArr[month] === 31){
                $('#weekend-day').html('{$days31}');
            }
            if(value && start){
                $('#weekend-day').val(value);
            }
        }
    }
    
    init(true);
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>