<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Weekend */
?>
<div class="weekend-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'month',
            'day',
        ],
    ]) ?>

</div>
