<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Weekend */
?>
<div class="weekend-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
