<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m200708_204903_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'vendor_code' => $this->integer()->comment('Артикул товара'),
            'balance_ignore' => $this->boolean()->comment('Не учитывать в балансовой прибили '),
            'name' => $this->string()->comment('Наименование '),
            'count' => $this->integer()->comment('количество'),
            'price' => $this->integer()->comment('Цена в руб за шт'),
            'sum' => $this->integer()->comment('сумма'),
            'rise' => $this->boolean()->comment('Увеличение (да/нет)'),
            'rise_sum' => $this->integer()->comment('Увеличение (количество)'),
            'availability' => $this->string()->comment('Наличие'),
            'buy_place' => $this->string()->comment('Где покупали'),
            'comment' => $this->text()->comment('Заметки'),
            'balance' => $this->string()->comment('Поле для отображения остатка в 1С'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-product-contract_id',
            'product',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-product-contract_id',
            'product',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-product-contract_id',
            'product'
        );
        $this->dropIndex(
            'idx-product-contract_id',
            'product'
        );
        $this->dropTable('product');
    }
}
