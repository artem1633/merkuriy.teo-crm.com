<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract`.
 */
class m200706_233455_create_contract_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contract', [
            'id' => $this->primaryKey(),
            'contract_type_id' => $this->integer()->comment('Тип Лота'),
            'for_small_business' => $this->boolean()->comment('Для субъекта малого предпринимательства'),
            'auction_number' => $this->string()->comment('№ Электронного аукциона'),
            'author_id' => $this->integer()->comment('кто создал'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-contract-contract_type_id',
            'contract',
            'contract_type_id'
        );
        $this->addForeignKey(
            'fk-contract-contract_type_id',
            'contract',
            'contract_type_id',
            'contract_type',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-contract-author_id',
            'contract',
            'author_id'
        );
        $this->addForeignKey(
            'fk-contract-author_id',
            'contract',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-contract-author_id',
            'contract'
        );
        $this->dropForeignKey(
            'fk-contract-contract_type_id',
            'contract'
        );


        $this->dropIndex(
            'idx-contract-contract_type_id',
            'contract'
        );
        $this->dropIndex(
            'idx-contract-author_id',
            'contract'
        );

        $this->dropTable('contract');
    }
}
