<?php

use yii\db\Migration;

/**
 * Class m200725_095651_update_cargo_ship_table
 */
class m200725_095651_update_cargo_ship_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cargo_ship', 'status', $this->string()->defaultValue(false)->comment('Статус отправки'));
        $this->addColumn('cargo_ship', 'receipt_date', $this->date()->defaultValue(false)->comment('Дата полученя груза заказчиком'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('cargo_ship', 'status');
        $this->dropColumn('cargo_ship', 'receipt_date');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_095651_update_cargo_ship_table cannot be reverted.\n";

        return false;
    }
    */
}
