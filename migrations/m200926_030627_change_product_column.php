<?php

use yii\db\Migration;

/**
 * Class m200926_030627_change_product_column
 */
class m200926_030627_change_product_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('product', 'vendor_code');
        $this->dropColumn('product', 'availability');
        $this->dropColumn('product', 'buy_place');
        $this->dropColumn('product', 'balance');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('product', 'vendor_code', $this->integer()->comment('Артикул товара'));
        $this->addColumn('product', 'availability', $this->string()->comment('Наличие'));
        $this->addColumn('product', 'buy_place', $this->string()->comment('Где покупали'));
        $this->addColumn('product', 'balance', $this->string()->comment('Поле для отображения остатка в 1С'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200926_030627_change_product_column cannot be reverted.\n";

        return false;
    }
    */
}
