<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_status`.
 */
class m200707_015742_create_order_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Статус заказа'),
            'date' => $this->date()->comment('Дата получения груза'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-order_status-contract_id',
            'order_status',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-order_status-contract_id',
            'order_status',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order_status-contract_id',
            'order_status'
        );
        $this->dropIndex(
            'idx-order_status-contract_id',
            'order_status'
        );
        $this->dropTable('order_status');
    }
}
