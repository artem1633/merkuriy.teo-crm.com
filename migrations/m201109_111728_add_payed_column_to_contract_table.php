<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contract}}`.
 */
class m201109_111728_add_payed_column_to_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contract', 'payed', $this->boolean()->defaultValue(false)->comment('Оплачено'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contract', 'payed');
    }
}
