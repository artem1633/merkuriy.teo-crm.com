<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract_filter`.
 */
class m201104_150628_create_contract_filter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contract_filter', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'order_status' => $this->string()->comment('Статус заказа'),
            'delivery_status' => $this->string()->comment('Статус поставки'),
            'shortorgname' => $this->string()->comment('Сокращенное наименование организации'),
            'contract_type_id' => $this->string()->comment('Тип Лота'),
            'auction_number' => $this->string()->comment('№ Электронного аукциона'),
        ]);

        $this->createIndex(
            'idx-contract_filter-user_id',
            'contract_filter',
            'user_id'
        );

        $this->addForeignKey(
            'fk-contract_filter-user_id',
            'contract_filter',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-contract_filter-user_id',
            'contract_filter'
        );

        $this->dropIndex(
            'idx-contract_filter-user_id',
            'contract_filter'
        );

        $this->dropTable('contract_filter');
    }
}
