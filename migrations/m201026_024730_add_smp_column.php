<?php

use yii\db\Migration;

/**
 * Class m201026_024730_add_smp_column
 */
class m201026_024730_add_smp_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('start_max_price', 'oik_type', $this->integer()->comment('Тип ОИК'));
        $this->addColumn('start_max_price', 'ogo_type', $this->integer()->comment('Тип ОГО'));
        $this->alterColumn('start_max_price','oz_percent' ,$this->float());
        $this->alterColumn('start_max_price','ogo_percent' ,$this->float());
        $this->alterColumn('start_max_price','oik_percent' ,$this->float());
        $this->alterColumn('start_max_price','ogo_contract' ,$this->float());
        $this->alterColumn('start_max_price','oik_contract' ,$this->float());
        $this->alterColumn('start_max_price','oik_mck' ,$this->float());
        $this->alterColumn('start_max_price','ogo_mck' ,$this->float());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('start_max_price', 'oik_type');
        $this->dropColumn('start_max_price', 'ogo_type');
        $this->alterColumn('start_max_price','oz_percent' ,$this->integer());
        $this->alterColumn('start_max_price','ogo_percent' ,$this->integer());
        $this->alterColumn('start_max_price','oik_percent' ,$this->integer());
        $this->alterColumn('start_max_price','ogo_contract' ,$this->integer());
        $this->alterColumn('start_max_price','oik_contract' ,$this->integer());
        $this->alterColumn('start_max_price','oik_mck' ,$this->integer());
        $this->alterColumn('start_max_price','ogo_mck' ,$this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201026_024730_add_smp_column cannot be reverted.\n";

        return false;
    }
    */
}
