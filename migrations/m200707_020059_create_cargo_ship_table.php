<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cargo_ship`.
 */
class m200707_020059_create_cargo_ship_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cargo_ship', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Груз'),
            'shipper_id' => $this->integer()->comment('Кем отправили'),
            'track' => $this->string()->comment('Ссылка или № трека'),
            'date' => $this->date()->comment('Дата'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-cargo_ship-contract_id',
            'cargo_ship',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-cargo_ship-contract_id',
            'cargo_ship',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-cargo_ship-shipper_id',
            'cargo_ship',
            'shipper_id'
        );
        $this->addForeignKey(
            'fk-cargo_ship-shipper_id',
            'cargo_ship',
            'shipper_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-cargo_ship-contract_id',
            'cargo_ship'
        );
        $this->dropIndex(
            'idx-cargo_ship-contract_id',
            'cargo_ship'
        );
        $this->dropForeignKey(
            'fk-cargo_ship-shipper_id',
            'cargo_ship'
        );
        $this->dropIndex(
            'idx-cargo_ship-shipper_id',
            'cargo_ship'
        );
        $this->dropTable('cargo_ship');
    }
}
