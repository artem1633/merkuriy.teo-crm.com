<?php

use yii\db\Migration;

/**
 * Class m200901_150226_add_changes1
 */
class m200901_150226_add_changes1 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('customer', 'purchase_name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('customer', 'purchase_name', $this->string()->comment('Наименование закупки'));
//

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200901_150226_add_changes1 cannot be reverted.\n";

        return false;
    }
    */
}
