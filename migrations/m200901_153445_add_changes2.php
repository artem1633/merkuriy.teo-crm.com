<?php

use yii\db\Migration;

/**
 * Class m200901_153445_add_changes2
 */
class m200901_153445_add_changes2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'purchase_name', $this->string()->comment('Наименование закупки'));
        $this->addColumn('customer', 'delivery_days', $this->integer()->comment('Срок поставки: дней'));
        $this->addColumn('customer', 'pay_days', $this->integer()->comment('Срок оплаты: дней'));
        $this->addColumn('customer', 'date_type', $this->integer()->comment('Тип Срока поставки'));

        $this->alterColumn('order_status', 'date', $this->dateTime()->comment('дата и время'));
        $this->alterColumn('order_status', 'created_at', $this->dateTime()->comment('дата и время созданя'));
        $this->alterColumn('order_status', 'updated_at', $this->dateTime()->comment('дата и время изменения'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'purchase_name');
        $this->dropColumn('customer', 'delivery_days');
        $this->dropColumn('customer', 'pay_days');
        $this->dropColumn('customer', 'date_type');

        $this->alterColumn('order_status', 'date', $this->date()->comment('дата'));
        $this->alterColumn('order_status', 'created_at', $this->date()->comment('дата  созданя'));
        $this->alterColumn('order_status', 'updated_at', $this->date()->comment('дата  изменения'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200901_153445_add_changes2 cannot be reverted.\n";

        return false;
    }
    */
}
