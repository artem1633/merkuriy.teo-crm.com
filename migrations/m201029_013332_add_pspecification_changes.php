<?php

use yii\db\Migration;

/**
 * Class m201029_013332_add_pspecification_changes
 */
class m201029_013332_add_pspecification_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('product_specification','sum' ,$this->float());
        $this->alterColumn('product_specification','price' ,$this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('product_specification','sum' ,$this->integer());
        $this->alterColumn('product_specification','price' ,$this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201029_013332_add_pspecification_changes cannot be reverted.\n";

        return false;
    }
    */
}
