<?php

use yii\db\Migration;

/**
 * Handles the creation of table `start_max_price`.
 */
class m200707_014418_create_start_max_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('start_max_price', [
            'id' => $this->primaryKey(),
            'contract_start_max_price' => $this->integer()->comment('НМЦК | Начальная Максимальная Цена Контракта:'),
            'oz' => $this->integer()->comment('ОЗ | Обеспечения Заявки: от НМЦК'),
            'oz_percent' => $this->integer()->comment('ОЗ | Обеспечения Заявки: %'),
            'oik_mck' => $this->integer()->comment('ОИК | Обеспечение Исполнения Контракта: от МЦК'),
            'oik_contract' => $this->integer()->comment('ОИК | Обеспечение Исполнения Контракта: от Контракта'),
            'oik_percent' => $this->integer()->comment('ОИК | Обеспечение Исполнения Контракта: %'),
            'ogo_mck' => $this->integer()->comment('ОГО | Обеспечение Гарантийных Обязательств: от МЦК'),
            'ogo_contract' => $this->integer()->comment('ОГО | Обеспечение Гарантийных Обязательств: от Контракта'),
            'ogo_percent' => $this->integer()->comment('ОГО | Обеспечение Гарантийных Обязательств: %'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-start_max_price-contract_id',
            'start_max_price',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-start_max_price-contract_id',
            'start_max_price',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-start_max_price-contract_id',
            'start_max_price'
        );
        $this->dropIndex(
            'idx-start_max_price-contract_id',
            'start_max_price'
        );
        $this->dropTable('start_max_price');
    }
}
