<?php

use yii\db\Migration;

/**
 * Class m200926_012148_change_customer_column
 */
class m200926_012148_change_customer_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'pay_type', $this->string()->comment('Тип оплаты'));
        $this->alterColumn('customer','pay_date',$this->date()->comment('Срок оплаты'));
        $this->alterColumn('customer','delivery_date',$this->date()->comment('Срок поставки'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'pay_type');
        $this->alterColumn('customer','pay_date',$this->dateTime()->comment('Срок оплаты'));
        $this->alterColumn('customer','delivery_date',$this->dateTime()->comment('Срок поставки'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200926_012148_change_customer_column cannot be reverted.\n";

        return false;
    }
    */
}
