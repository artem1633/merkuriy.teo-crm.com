<?php

use yii\db\Migration;

/**
 * Class m201013_135611_add_changes
 */
class m201013_135611_add_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contract', 'contract_stage');
        $this->dropColumn('contract', 'delivery_date');
        $this->dropColumn('contract', 'pay_date');
        $this->dropColumn('contract', 'pay_days_count');
        $this->dropColumn('contract', 'delivery_days_count');
        $this->alterColumn('start_max_price','contract_start_max_price' ,$this->decimal(12,2)->comment('НМЦК | Начальная Максимальная Цена Контракта:'));
        $this->addColumn('customer_contact', 'role', $this->string()->comment('Должность'));
    }

    /**
     * {@inheritdoc}
     */
        public function safeDown()
    {
        $this->addColumn('contract', 'contract_stage', $this->string()->comment('Стадия контракта'));
        $this->addColumn('contract', 'delivery_date', $this->date()->comment('Срок поставки до'));
        $this->addColumn('contract', 'pay_date', $this->date()->comment('Срок оплаты до'));
        $this->addColumn('contract', 'pay_days_count', $this->integer()->comment('Срок оплаты дней'));
        $this->addColumn('contract', 'delivery_days_count', $this->integer()->comment('Срок поставки дней'));
        $this->alterColumn('start_max_price','contract_start_max_price' ,$this->integer()->comment('НМЦК | Начальная Максимальная Цена Контракта:'));
        $this->dropColumn('customer_contact', 'role');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_135611_add_changes cannot be reverted.\n";

        return false;
    }
    */
}
