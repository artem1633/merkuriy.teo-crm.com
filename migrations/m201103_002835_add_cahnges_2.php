<?php

use yii\db\Migration;

/**
 * Class m201103_002835_add_cahnges_2
 */
class m201103_002835_add_cahnges_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('start_max_price','oz' ,$this->decimal(12,2));
        $this->alterColumn('order_status','date' ,$this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('start_max_price','oz' ,$this->integer());
        $this->alterColumn('order_status','date' ,$this->dateTime());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201103_002835_add_cahnges_2 cannot be reverted.\n";

        return false;
    }
    */
}
