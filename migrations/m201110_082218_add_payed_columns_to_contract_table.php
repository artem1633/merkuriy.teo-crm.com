<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contract}}`.
 */
class m201110_082218_add_payed_columns_to_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contract', 'payed_oik', $this->boolean()->defaultValue(false)->comment('Оплачено'));
        $this->addColumn('contract', 'payed_ogo', $this->boolean()->defaultValue(false)->comment('Оплачено'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contract', 'payed_oik');
        $this->dropColumn('contract', 'payed_ogo');
    }
}
