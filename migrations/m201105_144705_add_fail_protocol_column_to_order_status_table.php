<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_status}}`.
 */
class m201105_144705_add_fail_protocol_column_to_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_status', 'fail_protocol', $this->date()->comment('Протокол разногласий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_status', 'fail_protocol');
    }
}
