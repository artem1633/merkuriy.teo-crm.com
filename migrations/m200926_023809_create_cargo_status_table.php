<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cargo_status`.
 */
class m200926_023809_create_cargo_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cargo_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Статус заказа'),
            'date' => $this->dateTime()->comment('Дата получения груза'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-cargo_status-contract_id',
            'cargo_status',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-cargo_status-contract_id',
            'cargo_status',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-cargo_status-contract_id',
            'cargo_status'
        );
        $this->dropIndex(
            'idx-cargo_status-contract_id',
            'cargo_status'
        );
        $this->dropTable('cargo_status');
    }
}
