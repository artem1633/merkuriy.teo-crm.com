<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contract}}`.
 */
class m201211_130234_add_parsed_column_to_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contract', 'parsed', $this->boolean()->defaultValue(false)->comment('Данные спарсены'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contract', 'parsed');
    }
}
