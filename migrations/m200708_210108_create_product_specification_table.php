<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_specification`.
 */
class m200708_210108_create_product_specification_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_specification', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование '),
            'count' => $this->integer()->comment('Количество'),
            'price' => $this->integer()->comment('Цена в руб за шт'),
            'sum' => $this->integer()->comment('сумма'),
            'product_id' => $this->integer()->comment('id Продукта'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-product_specification-product_id',
            'product_specification',
            'product_id'
        );
        $this->addForeignKey(
            'fk-product_specification-product_id',
            'product_specification',
            'product_id',
            'product',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-product_specification-contract_id',
            'product_specification',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-product_specification-contract_id',
            'product_specification',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-product_specification-product_id',
            'product_specification'
        );
        $this->dropIndex(
            'idx-product_specification-product_id',
            'product_specification'
        );
        $this->dropTable('product_specification');
    }
}
