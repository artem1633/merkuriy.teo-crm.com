<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_contact`.
 */
class m200707_020744_create_customer_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_contact', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->comment('ФИО Контактного лица '),
            'email' => $this->string()->comment('Электронная почта'),
            'phone' => $this->string()->comment('Телефон'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-customer_contact-contract_id',
            'customer_contact',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-customer_contact-contract_id',
            'customer_contact',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer_contact-contract_id',
            'customer_contact'
        );
        $this->dropIndex(
            'idx-customer_contact-contract_id',
            'customer_contact'
        );
        $this->dropTable('customer_contact');
    }
}
