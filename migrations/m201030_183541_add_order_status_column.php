<?php

use yii\db\Migration;

/**
 * Class m201030_183541_add_order_status_column
 */
class m201030_183541_add_order_status_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_status', 'signature_deadline', $this->date());
        $this->addColumn('order_status', 'customer_sign_deadline', $this->date());
        $this->addColumn('order_status', 're_publish_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_status', 'signature_deadline');
        $this->dropColumn('order_status', 'customer_sign_deadline');
        $this->dropColumn('order_status', 're_publish_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201030_183541_add_order_status_column cannot be reverted.\n";

        return false;
    }
    */
}
