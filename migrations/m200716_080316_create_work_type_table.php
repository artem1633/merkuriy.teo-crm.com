<?php

use yii\db\Migration;

/**
 * Handles the creation of table `work_type`.
 */
class m200716_080316_create_work_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('work_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('work_type');
    }
}
