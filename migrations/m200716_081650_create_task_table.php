<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m200716_081650_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->comment('id контракта'),
            'work_type_id' => $this->integer()->comment('id вида работ'),
            'task_status_id' => $this->integer()->comment('id статуса'),
            'name' => $this->string()->comment('Описание задачи'),
            'date' => $this->date()->comment('Дата задачи'),
            'deadline' => $this->date()->comment('Срок окончания задачи'),
            'created_at' => $this->date()->comment('Дата создания'),
            'author_id' => $this->integer()->comment('Автор')
        ]);
        $this->createIndex(
            'idx-task-contract_id',
            'task',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-task-contract_id',
            'task',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-task-work_type_id',
            'task',
            'work_type_id'
        );
        $this->addForeignKey(
            'fk-task-work_type_id',
            'task',
            'work_type_id',
            'work_type',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-task-task_status_id',
            'task',
            'task_status_id'
        );
        $this->addForeignKey(
            'fk-task-task_status_id',
            'task',
            'task_status_id',
            'task_status',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-task-author_id',
            'task',
            'author_id'
        );
        $this->addForeignKey(
            'fk-task-author_id',
            'task',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task-author_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-author_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-task_status_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-task_status_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-work_type_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-work_type_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-contract_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-contract_id',
            'task'
        );
        $this->dropTable('task');
    }
}
