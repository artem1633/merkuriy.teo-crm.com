<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%weekend}}`.
 */
class m201105_105437_create_weekend_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%weekend}}', [
            'id' => $this->primaryKey(),
            'month' => $this->string()->comment('Месяц'),
            'day' => $this->string()->comment('День'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%weekend}}');
    }
}
