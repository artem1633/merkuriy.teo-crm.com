<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m200707_011914_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'full_name' => $this->string()->comment('Полное наименование организаци'),
            'short_name' => $this->string()->comment('Сокращенное наименование организаци'),
            'shipping_address' => $this->string()->comment('Адрес доставки'),
            'delivery_date' => $this->date()->comment('Срок поставки'),
            'pay_date' => $this->date()->comment('Срок оплаты'),
            'lot_link' => $this->string()->comment('Ссылка на Лот в системе поиска аукционов'),
            'lot_link_playground' => $this->string()->comment('Ссылка на лот в на площадке'),
            'lot_link_portal' => $this->string()->comment('Ссылка на лот на портале госзакупок'),
            'conducting_way' => $this->string()->comment('Способ проведения'),
            'deadline' => $this->date()->comment('Окончание сбора заявок'),
            'bid_consideration' => $this->date()->comment('Рассмотрение заявок'),
            'auction_date' => $this->date()->comment('Проведение аукциона'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения'),
            'contract_id' => $this->integer()->comment('id контракта'),
        ]);
        $this->createIndex(
            'idx-customer-contract_id',
            'customer',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-customer-contract_id',
            'customer',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer-contract_id',
            'customer'
        );
        $this->dropIndex(
            'idx-customer-contract_id',
            'customer'
        );
        $this->dropTable('customer');
    }
}
