<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bidding`.
 */
class m201022_004051_create_bidding_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bidding', [
            'id' => $this->primaryKey(),
            'bid_sum' => $this->decimal(12,2)->comment('Сумма Торгов'),
            'decline' => $this->boolean()->comment('Снижение > 25%'),
            'product_cost' => $this->decimal(12,2)->comment('Стоимость товара'),
            'profit' => $this->float()->comment('Прибыль'),
            'profit_percent' => $this->float()->comment('Прибыль в %'),
            'contract_id' => $this->integer()->comment('id контракта'),

        ]);
        $this->createIndex(
            'idx-bidding-contract_id',
            'bidding',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-bidding-contract_id',
            'bidding',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-bidding-contract_id',
            'bidding'
        );
        $this->dropIndex(
            'idx-bidding-contract_id',
            'bidding'
        );
        $this->dropTable('bidding');
    }
}
