<?php

use yii\db\Migration;

/**
 * Class m201005_084211_add_contract_column
 */
class m201005_084211_add_contract_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contract', 'contract_stage', $this->string()->comment('Стадия контракта'));
        $this->addColumn('contract', 'delivery_date', $this->date()->comment('Срок поставки до'));
        $this->addColumn('contract', 'pay_date', $this->date()->comment('Срок оплаты до'));
        $this->addColumn('contract', 'pay_days_count', $this->integer()->comment('Срок оплаты дней'));
        $this->addColumn('contract', 'delivery_days_count', $this->integer()->comment('Срок поставки дней'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contract', 'contract_stage');
        $this->dropColumn('contract', 'delivery_date');
        $this->dropColumn('contract', 'pay_date');
        $this->dropColumn('contract', 'pay_days_count');
        $this->dropColumn('contract', 'delivery_days_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201005_084211_add_contract_column cannot be reverted.\n";

        return false;
    }
    */
}
