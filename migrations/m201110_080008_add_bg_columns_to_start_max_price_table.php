<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%start_max_price}}`.
 */
class m201110_080008_add_bg_columns_to_start_max_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('start_max_price', 'oz_bg', $this->boolean()->comment('БГ'));
        $this->addColumn('start_max_price', 'oik_bg', $this->boolean()->comment('БГ'));
        $this->addColumn('start_max_price', 'ogo_bg', $this->boolean()->comment('БГ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('start_max_price', 'oz_bg');
        $this->dropColumn('start_max_price', 'oik_bg');
        $this->dropColumn('start_max_price', 'ogo_bg');
    }
}
