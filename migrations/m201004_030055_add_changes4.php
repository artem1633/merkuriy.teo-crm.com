<?php

use yii\db\Migration;

/**
 * Class m201004_030055_add_changes4
 */
class m201004_030055_add_changes4 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'link_1s', $this->string()->comment('ссылка на 1с'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'link_1s');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201004_030055_add_changes4 cannot be reverted.\n";

        return false;
    }
    */
}
