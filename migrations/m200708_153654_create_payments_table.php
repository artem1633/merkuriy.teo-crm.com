<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payments`.
 */
class m200708_153654_create_payments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'delivery_days' => $this->integer()->comment('Срок поставки: дней'),
            'pay_days' => $this->integer()->comment('Срок оплаты: дней'),
            'delivery_date' => $this->date()->comment('Срок поставки: дата'),
            'pay_date' => $this->date()->comment('Срок оплаты: дата'),
            'day_type' => $this->string()->comment('Тип дней'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-payments-contract_id',
            'payments',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-payments-contract_id',
            'payments',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-payments-contract_id',
            'payments'
        );
        $this->dropIndex(
            'idx-payments-contract_id',
            'payments'
        );
        $this->dropTable('payments');
    }
}
