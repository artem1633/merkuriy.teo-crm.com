<?php

use yii\db\Migration;

/**
 * Class m200926_023252_set_change_cargo_ship_column
 */
class m200926_023252_set_change_cargo_ship_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('cargo_ship', 'status');
        $this->dropColumn('cargo_ship', 'receipt_date');
        $this->dropForeignKey(
            'fk-cargo_ship-shipper_id',
            'cargo_ship'
        );
        $this->dropIndex(
            'idx-cargo_ship-shipper_id',
            'cargo_ship'
        );
        $this->dropColumn('cargo_ship', 'shipper_id');
        $this->addColumn('cargo_ship', 'shipper', $this->string()->comment('Кем отправили'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('cargo_ship', 'status', $this->string()->comment('Статус груза'));
        $this->addColumn('cargo_ship', 'receipt_date', $this->date()->comment('Дата получения груза заказчиком'));
        $this->dropColumn('cargo_ship', 'shipper');
        $this->addColumn('cargo_ship', 'shipper_id', $this->integer()->comment('Кем отправили'));
        $this->createIndex(
            'idx-cargo_ship-shipper_id',
            'cargo_ship',
            'shipper_id'
        );
        $this->addForeignKey(
            'fk-cargo_ship-shipper_id',
            'cargo_ship',
            'shipper_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200926_023252_set_change_cargo_ship_column cannot be reverted.\n";

        return false;
    }
    */
}
