<?php

use yii\db\Migration;

/**
 * Class m200725_105652_update_customer_table
 */
class m200725_105652_update_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'purchase_name', $this->string()->defaultValue(false)->comment('Наименование закупки'));
        $this->alterColumn('customer', 'deadline', $this->dateTime()->comment('Окончание сбора заявок'));
        $this->alterColumn('customer', 'auction_date', $this->dateTime()->comment('дата и время проведения аукциона'));
        $this->dropColumn('contract', 'for_small_business');
        $this->addColumn('customer', 'for_small_business', $this->boolean()->defaultValue(false)->comment('Для субъекта малого предпринимательства'));
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'purchase_name');
        $this->alterColumn('customer', 'deadline', $this->date()->comment('Окончание сбора заявок'));
        $this->alterColumn('customer', 'auction_date', $this->date()->comment('дата и время проведения аукциона'));
        $this->addColumn('contract', 'for_small_business', $this->boolean()->defaultValue(false)->comment('Для субъекта малого предпринимательства'));
        $this->dropColumn('customer', 'for_small_business');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_105652_update_customer_table cannot be reverted.\n";

        return false;
    }
    */
}
