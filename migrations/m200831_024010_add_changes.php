<?php

use yii\db\Migration;

/**
 * Class m200831_024010_add_changes
 */
class m200831_024010_add_changes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->alterColumn('customer', 'delivery_date', $this->dateTime()->comment('дата поставки'));
        $this->alterColumn('customer', 'pay_date', $this->dateTime()->comment('дата оплаты'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('customer', 'delivery_date', $this->date()->comment('дата поставки'));
        $this->alterColumn('customer', 'pay_date', $this->date()->comment('дата оплаты'));

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_024010_add_changes cannot be reverted.\n";

        return false;
    }
    */
}
