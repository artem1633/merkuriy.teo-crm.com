<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m200706_031614_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'email' => $this->string()->comment('email'),
            'phone' => $this->string()->comment('телефон'),
            'birth_date' => $this->date()->comment('дата рождения'),
            'avatar' => $this->string()->comment('Аватар'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'info' => $this->string()->comment('Информация'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'created_at' => $this->dateTime(),
        ]);

//
        $this->insert('user', [
            'login' => 'admin@admin.com',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
