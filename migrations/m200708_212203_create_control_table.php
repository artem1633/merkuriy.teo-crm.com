<?php

use yii\db\Migration;

/**
 * Handles the creation of table `control`.
 */
class m200708_212203_create_control_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('control', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->comment('id контракта'),
            'sum' => $this->integer()->comment('Сумма'),
            'checked' => $this->boolean()->comment('Проверено')
        ]);
        $this->createIndex(
            'idx-control-contract_id',
            'control',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-control-contract_id',
            'control',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-control-contract_id',
            'control'
        );
        $this->dropIndex(
            'idx-control-contract_id',
            'control'
        );
        $this->dropTable('control');
    }
}
