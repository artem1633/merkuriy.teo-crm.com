<?php

use yii\db\Migration;

/**
 * Handles the creation of table `costs`.
 */
class m200708_210625_create_costs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('costs', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'price' => $this->integer()->comment('Цена, руб'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения'),

        ]);
        $this->createIndex(
            'idx-costs-contract_id',
            'costs',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-costs-contract_id',
            'costs',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-costs-contract_id',
            'costs'
        );
        $this->dropIndex(
            'idx-costs-contract_id',
            'costs'
        );
        $this->dropTable('costs');
    }
}
