<?php

use yii\db\Migration;

/**
 * Class m201108_173357_add_cargo_status_changes
 */
class m201108_173357_add_cargo_status_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('cargo_status','date' ,$this->date());
        $this->addColumn('order_status', 'is_fair', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('cargo_status','date' ,$this->dateTime());
        $this->dropColumn('order_status', 'is_fair');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201108_173357_add_cargo_status_changes cannot be reverted.\n";

        return false;
    }
    */
}
