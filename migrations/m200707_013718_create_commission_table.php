<?php

use yii\db\Migration;

/**
 * Handles the creation of table `commission`.
 */
class m200707_013718_create_commission_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('commission', [
            'id' => $this->primaryKey(),
            'contract_nmck' => $this->integer()->comment('Контракт (НМЦК)'),
            'win_commission' => $this->integer()->comment('Комиссия за победу'),
            'oz_commission' => $this->integer()->comment('Комиссия за БГ | ОЗ'),
            'oik_commission' => $this->integer()->comment('Комиссия за БГ | ОИК '),
            'ogo_commission' => $this->integer()->comment('Комиссия за БГ | ОГО'),
            'contract_id' => $this->integer()->comment('id контракта'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-commission-contract_id',
            'commission',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-commission-contract_id',
            'commission',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-commission-contract_id',
            'commission'
        );
        $this->dropIndex(
            'idx-commission-contract_id',
            'commission'
        );
        $this->dropTable('commission');
    }
}
