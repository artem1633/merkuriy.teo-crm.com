<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract_type`.
 */
class m200706_233114_create_contract_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contract_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contract_type');
    }
}
