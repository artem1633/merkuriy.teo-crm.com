<?php

use yii\db\Migration;

/**
 * Handles the creation of table `calculation`.
 */
class m200708_211609_create_calculation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('calculation', [
            'id' => $this->primaryKey(),
            'price_all' => $this->integer()->comment('Стоимость всего товара:'),
            'commission_sum' => $this->integer()->comment('Сумма комиссий:'),
            'costs_sum' => $this->integer()->comment('Сумма доп. расходов:'),
            'percent' => $this->integer()->comment('Увелечение на %'),
            'profit' => $this->integer()->comment('Прибыль'),
            'balance_profit' => $this->integer()->comment('Балансовая прибыль'),
            'contract_id' => $this->integer()->comment('id контракта'),
        ]);
        $this->createIndex(
            'idx-calculation-contract_id',
            'calculation',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-calculation-contract_id',
            'calculation',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-calculation-contract_id',
            'calculation'
        );
        $this->dropIndex(
            'idx-calculation-contract_id',
            'calculation'
        );
        $this->dropTable('calculation');
    }
}
