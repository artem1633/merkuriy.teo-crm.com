<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Bidding]].
 *
 * @see Bidding
 */
class BiddingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Bidding[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Bidding|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
