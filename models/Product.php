<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $vendor_code Артикул товара
 * @property int $balance_ignore Не учитывать в балансовой прибили 
 * @property string $name Наименование 
 * @property int $count количество
 * @property int $price Цена в руб за шт
 * @property int $sum сумма
 * @property int $rise Увеличение (да/нет)
 * @property int $rise_sum Увеличение (количество)
 * @property string $availability Наличие
 * @property string $buy_place Где покупали
 * @property string $comment Заметки
 * @property string $balance Поле для отображения остатка в 1С
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property Contract $contract
 * @property ProductSpecification[] $productSpecifications
 */
class Product extends \yii\db\ActiveRecord
{
    public $balance;
    public $total;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'count', 'price', 'sum', 'rise', 'rise_sum', 'contract_id'], 'integer'],
            [[ 'count', 'price', 'sum'], 'required'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'vendor_code' => 'Артикул товара',
//            'balance_ignore' => 'БП ',
            'name' => 'Наименование ',
            'count' => 'количество',
            'price' => 'Цена в руб/шт',
            'sum' => 'сумма',
            'rise' => 'Увеличение (да/нет)',
            'rise_sum' => 'Увеличение (количество)',
//            'availability' => 'Наличие',
//            'buy_place' => 'Где покупали',
            'comment' => 'Заметки',
//            'balance' => 'Поле для отображения остатка в 1С',
            'contract_id' => 'id контракта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
            'balance' => 'Увеличение'
        ];
    }
    public function beforeDelete()
    {
        ProductSpecification::deleteAll(['product_id' => $this->id]);

        $bid = Bidding::find()->where(['contract_id' => $this->contract_id])->one();
        $bidSum = $bid->bid_sum;
        $sum = Product::find()->where(['contract_id' => $this->contract_id])->sum('sum');
        $product = Product::find()->where(['contract_id' => $this->contract_id])->andFilterWhere(['id'=>$this->id])->one();
        $sum = $sum - $product->sum;
        $profit =  $bidSum - $sum;
        $profitPercent = round((($bidSum/$sum)*100)-100,2);
        $bid->product_cost = $sum;
        $bid->profit = $profit;
        $bid->profit_percent = $profitPercent;
        $bid->save(false);
        return parent::beforeDelete();
    }
    public function afterSave($insert, $changedAttributes)
    {

        $bid = Bidding::find()->where(['contract_id' => $this->contract_id])->one();
//        $bidSum = $bid->bid_sum;
//        $sumCount = Product::find()->where(['contract_id' => $this->contract_id])->sum('sum');
//
//        $sum = $sumCount;
//        $profit =  $bidSum - $sum;
//        $profitPercent = round((($bidSum/$sum)*100)-100,2);
//
//        $bid->product_cost = $sum;
//        $bid->profit = $profit;
//        $bid->profit_percent = $profitPercent;
        $bid->save();

//        if ($insert) {
//            if ($bidSum == 0.00){
//                (new ProductSpecification([
//                    'product_id' => $this->id,
//                    'name' => $this->name,
//                    'count' => $this->count,
//                    'price' => $this->price,
//                    'sum' => $this->sum,
//                    'contract_id' => $this->contract_id
//                ]))->save(false);
//            }else{
//                $risePercent = ($bidSum - $sumCount) / $sumCount;
//                $price = round((($this->price / 100) * $risePercent) + $this->price, 2);
//                $specificationSum = $price * $this->count;
//                (new ProductSpecification([
//                    'product_id' => $this->id,
//                    'name' => $this->name,
//                    'count' => $this->count,
//                    'price' => $price,
//                    'sum' => $specificationSum,
//                    'contract_id' => $this->contract_id
//                ]))->save(false);
//            }
//
//        }else{
//            if ($bidSum == 0.00){
//                if ($this->rise_sum !== null && $this->rise_sum !== ""){
//                    $count = $this->count + $this->rise_sum;
//                }else{
//                    $count = $this->count;
//                }
//                $spec = ProductSpecification::find()->where(['product_id'=>$this->id])->one();
//                $spec->price = $this->price;
//                $spec->sum = $this->sum;
//                $spec->count = $count;
//                $spec->name = $this->name;
//                $spec->save(false);
//            }else{
//                $risePercent = ($bidSum - $sumCount)/$sumCount;
//                $price = round((($this->price/100)*$risePercent) + $this->price,2);
//
//                if ($this->rise_sum !== null && $this->rise_sum !== ""){
//                    $specificationSum = $price * ($this->count + $this->rise_sum);
//                    $count = $this->count + $this->rise_sum;
//                }else{
//                    $specificationSum = $price * $this->count;
//                    $count = $this->count;
//                }
//
//                $spec = ProductSpecification::find()->where(['product_id'=>$this->id])->one();
//                $spec->price = $price;
//                $spec->sum = $specificationSum;
//                $spec->count = $count;
//                $spec->name = $this->name;
//                $spec->save(false);
//            }
//
//
//        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }
    public static function getTotal($contract_id)
    {
        $total = 0;
        $total = Product::find()->where(['contract_id' => $contract_id])->count('sum');
        return $total;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSpecifications()
    {
        return $this->hasMany(ProductSpecification::className(), ['product_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
