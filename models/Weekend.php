<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weekend".
 *
 * @property int $id
 * @property string $month Месяц
 * @property string $day День
 */
class Weekend extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weekend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['month', 'day'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month' => 'Месяц',
            'day' => 'День',
        ];
    }
}
