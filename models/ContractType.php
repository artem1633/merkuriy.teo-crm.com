<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contract_type".
 *
 * @property int $id
 * @property string $name Название
 *
 * @property Contract[] $contracts
 */
class ContractType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['contract_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ContractTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContractTypeQuery(get_called_class());
    }
}
