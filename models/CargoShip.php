<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cargo_ship".
 *
 * @property int $id
 * @property int $name Груз
 * @property string $shipper Кем отправили
 * @property string $track Ссылка или № трека
 * @property string $date Дата
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 * @property string $status статус
 * @property string $receipt_date дата получения
 *
 * @property Contract $contract

 */
class CargoShip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cargo_ship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'contract_id'], 'integer'],
            [['date', 'created_at', 'shipper','updated_at'], 'safe'],
            [['track'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Груз',
            'shipper' => 'Кем отправили',
            'track' => 'Ссылка или № трека',
            'date' => 'Дата',
            'contract_id' => 'id контракта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
        ];

    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipper()
    {
        return $this->hasOne(User::className(), ['id' => 'shipper_id']);
    }

    /**
     * @inheritdoc
     * @return CargoShipQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CargoShipQuery(get_called_class());
    }
}
