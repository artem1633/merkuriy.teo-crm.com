<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "costs".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $price Цена, руб
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property Contract $contract
 */
class Costs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'costs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'contract_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'price' => 'Цена, руб',
            'contract_id' => 'id контракта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $bid = Bidding::find()->where(['contract_id' => $this->contract_id])->one();
        $bid->save(false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return CostsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CostsQuery(get_called_class());
    }
}
