<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contract;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContractSearch represents the model behind the search form about `app\models\Contract`.
 */
class ContractSearch extends Contract
{
    /** @var int */
    public $filterId;

    /** @var string */
    public $filterName;

    /** @var string */
    public $shortOrgName;

    /** @var string */
    public $deliv_status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','author_id', 'filterId'], 'integer'],
            [['auction_number', 'created_at','updated_at','order_status','deliv_status','fullName', 'filterName', 'contract_type_id', 'shortOrgName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contract::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $sort = $dataProvider->getSort();
        $dataProvider->setSort([
            'attributes' => [
                'id',
            ]
        ]);


//        if (!($this->load($params) && $this->validate())) {
//            /**
//             * Жадная загрузка данных модели Страны
//             * для работы сортировки.
//             */
//
//            return $dataProvider;
//        }

        $query->joinWith(['orderStatus', 'deliveryStatus', 'customer']);

//        $query->joinWith(['orderStatus'=> function ($q) {
//            $q->andFilterWhere(['like', 'order_status.name', $this->order_status]);
//        }]);
//        $query->joinWith(['deliveryStatus'=> function ($q) {
//        $q->andFilterWhere(['like', 'cargo_status.name', $this->deliveryStatus]);
//    }]);
//
//        $query->joinWith(['customer'=> function ($q) {
//            $q->andWhere('full_name LIKE "%'. $this->fullName . '%" ' .
//                'OR short_name LIKE "%' . $this->fullName . '%"' . 'OR inn LIKE "%' . $this->fullName . '%"'
//);
////                ['like', 'customer.fullName', $this->fullName]);
//        }]);



        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'contract_type_id' => $this->contract_type_id,
            'order_status.name' => $this->order_status,
        ]);


        $query->andFilterWhere(['cargo_status.name' => $this->deliv_status]);

        $query->andFilterWhere(['like', 'auction_number', $this->auction_number]);

        $inn = null;

        $oldShortName = $this->shortOrgName;

        if(strstr($this->shortOrgName, '( ИНН-')){
            $innArr = explode(' ( ИНН-', $this->shortOrgName);
            if(isset($innArr[1])){
                $innArr = explode(' ', $innArr[1]);
                if(isset($innArr[0])){
                    $inn = $innArr[0];
                    $this->shortOrgName = $innArr[0];
                }
            }
            $query->andFilterWhere(['or', ['like', 'customer.short_name', $this->shortOrgName], ['like', 'customer.full_name', $this->shortOrgName], ['customer.inn' => [$inn, $this->shortOrgName]], ['auction_number' => $this->shortOrgName]]);
            $this->shortOrgName = $oldShortName;
        } else {
            $query->andFilterWhere(['or', ['like', 'customer.short_name', $this->shortOrgName], ['like', 'customer.full_name', $this->shortOrgName], ['customer.inn' => $this->shortOrgName], ['auction_number' => $this->shortOrgName]]);
        }




        return $dataProvider;
    }
}
