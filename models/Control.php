<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "control".
 *
 * @property int $id
 * @property int $contract_id id контракта
 * @property int $sum Сумма
 * @property int $checked Проверено
 *
 * @property Contract $contract
 */
class Control extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_id', 'sum', 'checked'], 'integer'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'id контракта',
            'sum' => 'Сумма',
            'checked' => 'Проверено',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return ControlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ControlQuery(get_called_class());
    }
}
