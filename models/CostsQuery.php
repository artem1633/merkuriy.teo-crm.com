<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Costs]].
 *
 * @see Costs
 */
class CostsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Costs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Costs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
