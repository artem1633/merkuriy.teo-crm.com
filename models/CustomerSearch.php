<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['inn', 'kpp', 'full_name', 'short_name', 'shipping_address', 'delivery_date', 'pay_date','date_type',
                'pay_days','date_type','delivery_days','pay_type',
                'lot_link', 'lot_link_playground', 'lot_link_portal', 'conducting_way', 'deadline', 'bid_consideration',
                 'auction_date', 'created_at', 'updated_at','for_small_business','purchase_name','link_1s'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'for_small_business' => $this->for_small_business,
            'delivery_date' => $this->delivery_date,
            'pay_date' => $this->pay_date,
            'deadline' => $this->deadline,
            'bid_consideration' => $this->bid_consideration,
            'auction_date' => $this->auction_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'date_type' => $this->date_type,
            'pay_type' => $this->pay_type,
            'delivery_days' => $this->$this->delivery_days,
            'pay_days' => $this->pay_days,

        ]);

        $query->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'shipping_address', $this->shipping_address])
            ->andFilterWhere(['like', 'lot_link', $this->lot_link])
            ->andFilterWhere(['like', 'lot_link_playground', $this->lot_link_playground])
            ->andFilterWhere(['like', 'lot_link_portal', $this->lot_link_portal])
            ->andFilterWhere(['like', 'conducting_way', $this->conducting_way])
            ->andFilterWhere(['like', 'purchase_name' => $this->purchase_name])
            ->andFilterWhere(['like', 'link_1s' => $this->link_1s])
        ;

        return $dataProvider;
    }
}
