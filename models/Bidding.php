<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bidding".
 *
 * @property int $id
 * @property int $bid_sum Сумма Торгов
 * @property int $decline Снижение > 25%
 * @property int $product_cost Стоимость товара
 * @property int $profit Прибыль
 * @property int $profit_percent Прибыль в %
 * @property int $contract_id id контракта
 *
 * @property Contract $contract
 */
class Bidding extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bidding';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['decline', 'contract_id'], 'integer'],
            [['bid_sum','product_cost', 'profit', 'profit_percent',  ],'safe'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bid_sum' => 'Сумма Торгов',
            'decline' => 'Снижение > 25%',
            'product_cost' => 'Стоимость товара',
            'profit' => 'Прибыль',
            'profit_percent' => 'Прибыль в %',
            'contract_id' => 'id контракта',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (Product::find()->where(['contract_id' => $this->contract_id])->exists()){
            $bidSum = $this->bid_sum;
            if($bidSum > 0){
                $bidSumPercent = $bidSum / 100;
                $productSum = Product::find()->where(['contract_id' => $this->contract_id])->sum('sum');
                $costs = Costs::find()->where(['contract_id' => $this->contract_id])->sum('price');
                $profit = $bidSum - ($productSum + $costs);
                $q1 = ($bidSum - $productSum)/$productSum;
//            $profit = $profit - $costs;
//            $profitPercent = (($bidSum/$productSum)*100)-100;
//            $this->product_cost = $this->product_cost + $costs;
                $profitPercent = $profit / $bidSumPercent;
                $this->profit = $profit;
                $this->profit_percent = $profitPercent;

                /** @var Product[] $products */
                $products = Product::find()->where(['contract_id' => $this->contract_id])->all();
                $productsSum = array_sum(ArrayHelper::getColumn($products, 'sum'));
                $productsSumPercent = $productsSum / 100;

//                $specifications = ProductSpecification::find()->where(['contract_id' => $this->contract_id])->all();

                $this->product_cost = $productSum;

                ProductSpecification::deleteAll(['contract_id' => $this->contract_id]);

                foreach ($products as $product){
                    $specification = new ProductSpecification([
                        'product_id' => $product->id,
                        'name' => $product->name,
                        'count' =>  $product->count,
                        // 'count' => $product->rise ? ($product->rise_sum + $product->count) : $product->count,
//                        'price' => $product->price,
//                        'sum' => $product->sum,
                        'contract_id' => $product->contract_id
                    ]);
                            if($bidSum == $this->product_cost){
                                $specification->price = $product->price;
                                $specification->sum = $product->sum;
                                $specification->save(false);
                            } else {
                                // $percent = $product->sum / $productsSumPercent;
                                // $specification->sum = $percent * $bidSumPercent;
                                // $specification->price = $specification->sum / $specification->count;

                                // $percent = $product->sum / $productsSumPercent;
                                // $specification->sum = $percent * $bidSumPercent;
                                $specification->price = $product->price * $q1 + $product->price;
                                $specification->sum = $specification->price * $specification->count;
//                                $specification->price =
                                $specification->save(false);
                            }
                            // 700 - 1% = 7. 500 / 7 = 71%
                }

                $this->contract->save(false);
            }

        }


        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * {@inheritdoc}
     * @return BiddingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BiddingQuery(get_called_class());
    }
}
