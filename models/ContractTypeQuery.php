<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ContractType]].
 *
 * @see ContractType
 */
class ContractTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ContractType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ContractType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
