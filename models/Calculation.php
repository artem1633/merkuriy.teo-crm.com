<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calculation".
 *
 * @property int $id
 * @property int $price_all Стоимость всего товара:
 * @property int $commission_sum Сумма комиссий:
 * @property int $costs_sum Сумма доп. расходов:
 * @property int $percent Увелечение на %
 * @property int $profit Прибыль
 * @property int $balance_profit Балансовая прибыль
 * @property int $contract_id id контракта
 *
 * @property Contract $contract
 */
class Calculation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calculation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_all', 'commission_sum', 'costs_sum', 'percent', 'profit', 'balance_profit', 'contract_id'], 'integer'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_all' => 'Стоимость всего товара:',
            'commission_sum' => 'Сумма комиссий:',
            'costs_sum' => 'Сумма доп. расходов:',
            'percent' => 'Увелечение на %',
            'profit' => 'Прибыль',
            'balance_profit' => 'Балансовая прибыль',
            'contract_id' => 'id контракта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return CalculationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CalculationQuery(get_called_class());
    }
}
