<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[StartMaxPrice]].
 *
 * @see StartMaxPrice
 */
class StartMaxPriceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StartMaxPrice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StartMaxPrice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
