<?php

namespace app\models;

use DatePeriod;
use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order_status".
 *
 * @property int $id
 * @property string $name Статус заказа
 * @property string $date Дата получения груза
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 * @property string $signature_deadline дата подписи
 * @property string $customer_sign_deadline
 * @property string $re_publish_date
 * @property string $fail_protocol
 * @property boolean $is_fair
 *
 *
 * @property Contract $contract
 */
class OrderStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at','signature_deadline','customer_sign_deadline','re_publish_date','fail_protocol','is_fair'], 'safe'],
            [['contract_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Статус заказа',
            'date' => 'Дата',
            'contract_id' => 'id контракта',
            'created_at' => 'дата и время созданя',
            'updated_at' => 'дата и время изменения',
            'signature_deadline' => 'Мы подписать до',
            'customer_sign_deadline' => 'Заказчик подписать до',
            're_publish_date' => 'Контракт размещен повторно до',
            'fail_protocol' => 'Протокол разногласий',
            'is_fair' => 'При подписании контракта подтвердил добросовестность'
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }
    public function beforeSave($insert)
    {
        if ($this->is_fair == 1){
            $smp = StartMaxPrice::find()->where(['contract_id' => $this->contract_id])->one();
            $smp->oz = 0;
            $smp->oz_percent = 0;
            $smp->oik_contract = 0;
            $smp->oik_mck = 0;
            $smp->oik_percent = 0;
            $smp->ogo_contract = 0;
            $smp->ogo_percent = 0;
            $smp->ogo_mck = 0;
            $smp->save(false);
        }

        if($this->name == null){
            $this->name = 'Подача заявок';
        }

        return parent::beforeSave($insert);

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return OrderStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderStatusQuery(get_called_class());
    }
}
