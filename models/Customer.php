<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property string $full_name Полное наименование организаци
 * @property string $short_name Сокращенное наименование организаци
 * @property string $shipping_address Адрес доставки
 * @property string $delivery_date Срок поставки
 * @property string $pay_date Срок оплаты
 * @property string $lot_link Ссылка на Лот в системе поиска аукционов
 * @property string $lot_link_playground Ссылка на лот в на площадке
 * @property string $lot_link_portal Ссылка на лот на портале госзакупок
 * @property string $conducting_way Способ проведения
 * @property string $deadline Окончание сбора заявок
 * @property string $bid_consideration Рассмотрение заявок
 * @property string $auction_date Проведение аукциона
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 * @property string $for_small_business дата изменения
 * @property string $purchase_name Наименование закупки
 * @property integer $date_type Тип Срока поставки
 * @property integer $delivery_days доставка днейlot_link_playground
 * @property integer $pay_days оплата дней
 * @property integer $pay_type Тип оплаты
 * @property string $link_1s ссылка 1с
 *
 *
 *

 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_days','date_type','delivery_days','pay_type'],'integer'],
            [[
                'auction_date',
                'bid_consideration',
                'deadline',
                'conducting_way',
                'shipping_address',
                'full_name',
                'short_name',
                'inn',
                'kpp',
            ], 'required'],
            [['delivery_date', 'pay_date', 'deadline', 'bid_consideration', 'auction_date', 'created_at', 'updated_at','for_small_business','purchase_name','link_1s'], 'safe'],
            [['inn', 'kpp', 'full_name', 'short_name', 'shipping_address', 'lot_link', 'lot_link_playground', 'lot_link_portal', 'conducting_way'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'full_name' => 'Полное наименование организации',
            'short_name' => 'Сокращенное наименование организации',
            'shipping_address' => 'Адрес доставки',
            'delivery_date' => 'Срок поставки',
            'pay_date' => 'Срок оплаты',
            'lot_link' => 'Ссылка на Лот в системе поиска аукционов',
            'lot_link_playground' => 'Ссылка на лот в на площадке',
            'lot_link_portal' => 'Ссылка на лот на портале госзакупок',
            'conducting_way' => 'Способ проведения',
            'deadline' => 'Окончание сбора заявок',
            'bid_consideration' => 'Рассмотрение заявок',
            'auction_date' => 'Проведение аукциона',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
            'for_small_business' => 'Для субъекта малого предпринимательства',
            'purchase_name' => 'Наименование закупки',
            'date_type' => 'Тип Срока поставки',
            'pay_days' => 'Срок оплаты',
            'delivery_days' =>'Срок поставки',
            'pay_type' => 'Тип оплаты',
            'link_1s' => 'Ссылка на 1с'

        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }
    /**
     * @return string
     */
    public function getFullName() {
        return $this->full_name . ' ' . $this->short_name . ' ' . $this->inn;
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
