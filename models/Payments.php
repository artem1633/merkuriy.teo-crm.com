<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $delivery_days Срок поставки: дней
 * @property int $pay_days Срок оплаты: дней
 * @property string $delivery_date Срок поставки: дата
 * @property string $pay_date Срок оплаты: дата
 * @property string $day_type Тип дней
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property Contract $contract
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_days', 'pay_days', 'contract_id'], 'integer'],
            [['delivery_date', 'pay_date', 'created_at', 'updated_at'], 'safe'],
            [['day_type'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_days' => 'Срок поставки: дней',
            'pay_days' => 'Срок оплаты: дней',
            'delivery_date' => 'Срок поставки: дата',
            'pay_date' => 'Срок оплаты: дата',
            'day_type' => 'Тип дней',
            'contract_id' => 'id контракта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentsQuery(get_called_class());
    }
}
