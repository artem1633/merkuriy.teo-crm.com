<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contract_filter".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $user_id Пользователь
 * @property string $order_status Статус заказа
 * @property string $delivery_status Статус поставки
 * @property string $shortorgname Сокращенное наименование организации
 * @property string $contract_type_id Тип Лота
 * @property string $auction_number № Электронного аукциона
 *
 * @property User $user
 */
class ContractFilter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'order_status', 'delivery_status', 'shortorgname', 'contract_type_id', 'auction_number'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'user_id' => 'Пользователь',
            'order_status' => 'Статус заказа',
            'delivery_status' => 'Статус поставки',
            'shortorgname' => 'Сокращенное наименование организации',
            'contract_type_id' => 'Тип Лота',
            'auction_number' => '№ Электронного аукциона',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
