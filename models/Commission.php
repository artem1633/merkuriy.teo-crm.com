<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "commission".
 *
 * @property int $id
 * @property int $contract_nmck Контракт (НМЦК)
 * @property int $win_commission Комиссия за победу
 * @property int $oz_commission Комиссия за БГ | ОЗ
 * @property int $oik_commission Комиссия за БГ | ОИК 
 * @property int $ogo_commission Комиссия за БГ | ОГО
 * @property int $contract_id id контракта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property Contract $contract
 */
class Commission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_nmck', 'win_commission', 'oz_commission', 'oik_commission', 'ogo_commission', 'contract_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_nmck' => 'Контракт (НМЦК)',
            'win_commission' => 'Комиссия за победу',
            'oz_commission' => 'Комиссия за БГ | ОЗ',
            'oik_commission' => 'Комиссия за БГ | ОИК ',
            'ogo_commission' => 'Комиссия за БГ | ОГО',
            'contract_id' => 'id контракта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @inheritdoc
     * @return CommissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommissionQuery(get_called_class());
    }
}
