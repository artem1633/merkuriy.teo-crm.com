<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calculation;

/**
 * CalculationSearch represents the model behind the search form about `app\models\Calculation`.
 */
class CalculationSearch extends Calculation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price_all', 'commission_sum', 'costs_sum', 'percent', 'profit', 'balance_profit', 'contract_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calculation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price_all' => $this->price_all,
            'commission_sum' => $this->commission_sum,
            'costs_sum' => $this->costs_sum,
            'percent' => $this->percent,
            'profit' => $this->profit,
            'balance_profit' => $this->balance_profit,
            'contract_id' => $this->contract_id,
        ]);

        return $dataProvider;
    }
}
