<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CargoShip;

/**
 * CargoShipSearch represents the model behind the search form about `app\models\CargoShip`.
 */
class CargoShipSearch extends CargoShip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name','contract_id'], 'integer'],
            [['track', 'date', 'created_at', 'shipper',  'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CargoShip::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'shipper' => $this->shipper,
            'date' => $this->date,
            'contract_id' => $this->contract_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'track', $this->track]);

        return $dataProvider;
    }
}
