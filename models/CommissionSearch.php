<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Commission;

/**
 * CommissionSearch represents the model behind the search form about `app\models\Commission`.
 */
class CommissionSearch extends Commission
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_nmck', 'win_commission', 'oz_commission', 'oik_commission', 'ogo_commission', 'contract_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Commission::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contract_nmck' => $this->contract_nmck,
            'win_commission' => $this->win_commission,
            'oz_commission' => $this->oz_commission,
            'oik_commission' => $this->oik_commission,
            'ogo_commission' => $this->ogo_commission,
            'contract_id' => $this->contract_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
