<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_specification".
 *
 * @property int $id
 * @property string $name Наименование 
 * @property int $count Количество
 * @property int $price Цена в руб за шт
 * @property int $sum сумма
 * @property int $product_id id Продукта
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property Product $product
 */
class ProductSpecification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_specification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'price', 'sum', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование ',
            'count' => 'Количество',
            'price' => 'Цена в руб за шт',
            'sum' => 'сумма',
            'product_id' => 'id Продукта',
            'created_at' => 'дата созданя',
            'updated_at' => 'дата изменения',
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return ProductSpecificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductSpecificationQuery(get_called_class());
    }
}
