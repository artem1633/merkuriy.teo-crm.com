<?php

namespace app\models;

use app\helpers\ZakupkiParser;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contract".
 *
 * @property int $id
 * @property int $contract_type_id Тип Лота
 * @property int $author_id автор
 * @property int $for_small_business Для субъекта малого предпринимательства
 * @property string $auction_number № Электронного аукциона
 * @property string $created_at
 * @property string $updated_at
 * @property string $order_status
 * @property string $deliveryStatus
 * @property string $shortOrgName
 * @property string $price
 * @property string $fullName
 * @property int $payed
 * @property int $payed_oik
 * @property int $payed_ogo
 * @property int $parsed
 */
class Contract extends \yii\db\ActiveRecord
{
    public $order_status;
    public $deliveryStatus;
    public $price;
    public $shortOrgName;
    public $fullName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_type_id', 'payed', 'payed_oik', 'payed_ogo', 'parsed'], 'integer'],
            [['created_at','updated_at',], 'safe'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['auction_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'author_id',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * @inheritdoc
     */
  public function afterSave($insert, $changedAttributes)
  {
      parent::afterSave($insert, $changedAttributes);

      if ($insert){
          (new Calculation([
              'contract_id' => $this->id
          ]))->save(false);

          (new Control([
              'contract_id' => $this->id
          ]))->save(false);

          (new OrderStatus([
              'contract_id' => $this->id
          ]))->save(false);

          (new CargoStatus([
              'contract_id' => $this->id
          ]))->save(false);

          (new Customer([
              'contract_id' => $this->id
          ]))->save(false);

          (new Commission([
              'contract_id' => $this->id
          ]))->save(false);

          (new StartMaxPrice([
              'contract_id' => $this->id
          ]))->save(false);
          (new Bidding([
              'contract_id' => $this->id,
              'bid_sum' => 0
          ]))->save(false);

          $this->loadInfo();
      }


  }
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $statuses  = OrderStatus::find()->where(['contract_id' => $this->id])->all();
        foreach ($statuses as $status){
            if ($status->name == 'Проиграли'){
                Product::deleteAll(['contract_id' => $status->contract_id]);
                CargoStatus::deleteAll(['contract_id' => $status->contract_id]);
                Calculation::deleteAll(['contract_id' => $status->contract_id]);
                CargoShip::deleteAll(['contract_id' => $status->contract_id]);
                Commission::deleteAll(['contract_id' => $status->contract_id]);
                Control::deleteAll(['contract_id' => $status->contract_id]);
                Costs::deleteAll(['contract_id' => $status->contract_id]);
                Customer::deleteAll(['contract_id' => $status->contract_id]);
                CustomerContact::deleteAll(['contract_id' => $status->contract_id]);
                OrderStatus::deleteAll(['contract_id' => $status->contract_id]);
                Payments::deleteAll(['contract_id' => $status->contract_id]);
                StartMaxPrice::deleteAll(['contract_id' => $status->contract_id]);
                Bidding::deleteAll(['contract_id' => $status->contract_id]);
                ProductSpecification::deleteAll(['contract_id' => $status->contract_id]);
            }else{
                return false;
            }

        }



        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_type_id' => 'Тип Лота',
            'auction_number' => '№ Электронного аукциона',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'author_id' => 'author',
            'price' => 'Цена',
            'order_status' => 'Статус заказа',
            'order_date' => 'Дата статуса заказа',
            'product_delivery_date' => 'Дата поставки',
            'shortOrgName' => 'Сокращенное наименование организации',
            'deliveryStatus' => 'Статус поставки',
            'payed' => 'Оплачен',
            'payed_oik' => 'Оплачен',
            'payed_ogo' => 'Оплачен',
            'fullName' => 'Сокращенное наименование организации (ИНН)',
            'parsed' => 'Данные загружены',
        ];
    }

    /**
     */
    public function loadInfo()
    {
        $loadData = ZakupkiParser::parse($this->auction_number);
        if($loadData == false){
            $this->parsed = 0;
            $this->save(false);
        }
        $this->parsed = 1;

        $customer = Customer::find()->where(['contract_id' => $this->id])->one();

        if($customer){
            $customer->lot_link_portal = 'https://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber='.$this->auction_number;
            $customer->full_name = ArrayHelper::getValue($loadData, 'Организация, осуществляющая размещение');
//            $customer->shipping_address = ArrayHelper::getValue($loadData, 'Место доставки товара, выполнения работы или оказания услуги');
            $customer->purchase_name = ArrayHelper::getValue($loadData, 'Наименование объекта закупки');
//            $customer->purchase_name = ArrayHelper::getValue($loadData, 'Наименование объекта закупки');
            $customer->inn = ArrayHelper::getValue($loadData, 'ИНН');
            $customer->kpp = ArrayHelper::getValue($loadData, 'КПП');


            //Дата проведения аукциона в электронной форме

            $dateValue = ArrayHelper::getValue($loadData, 'Дата и время окончания срока подачи заявок на участие в электронном аукционе');
            if($dateValue != null){
                $dateAndTime = explode(' ', $dateValue);


                if(count($dateAndTime) == 2){
                    $dateValue = $dateAndTime[0];
                    $dateValue = explode('.', $dateValue);
                    if(count($dateValue) == 3){
                        $d = $dateValue[0];
                        $m = $dateValue[1];
                        $y = $dateValue[2];


                        $customer->deadline = "{$y}-{$m}-{$d} ".$dateAndTime[1];
                    }
                }
            }

            $dateValue = ArrayHelper::getValue($loadData, 'Дата проведения аукциона в электронной форме');
            if($dateValue != null){
                $dateValue = explode('.' , $dateValue);
                if(count($dateValue) == 3){
                    $d = $dateValue[0];
                    $m = $dateValue[1];
                    $y = $dateValue[2];

                    $customer->auction_date = "{$y}-{$m}-{$d} ".ArrayHelper::getValue($loadData, 'Время проведения аукциона');
                }
            }

            $customer->save(false);
        }

        $startMaxPrice = StartMaxPrice::find()->where(['contract_id' => $this->id])->one();


        if($startMaxPrice){
            \Yii::warning(ArrayHelper::getValue($loadData, 'Начальная (максимальная) цена контракта'));
            $startMaxPriceValue = ArrayHelper::getValue($loadData, 'Начальная (максимальная) цена контракта');
            $startMaxPriceValue = htmlentities($startMaxPriceValue);
            \Yii::warning($startMaxPriceValue);
            $startMaxPriceValue = str_replace(',', '.', str_replace('&nbsp;', '', $startMaxPriceValue));
            \Yii::warning($startMaxPriceValue);
            $startMaxPrice->contract_start_max_price = $startMaxPriceValue;


//            Размер обеспечения исполнения контракта
            //oik_contract

            $startMaxPriceValue = ArrayHelper::getValue($loadData, 'Размер обеспечения исполнения контракта');
            $startMaxPriceValue = htmlentities($startMaxPriceValue);
            \Yii::warning($startMaxPriceValue);
            $startMaxPriceValue = str_replace(',', '.', str_replace('&nbsp;', '', $startMaxPriceValue));
            $startMaxPriceValue = str_replace('Российский рубль', '', $startMaxPriceValue);
            \Yii::warning($startMaxPriceValue);
            $startMaxPrice->oik_contract = $startMaxPriceValue;

            $startMaxPrice->save(false);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['contract_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryStatus()
    {
        return $this->hasOne(CargoStatus::className(), ['contract_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractType()
    {
        return $this->hasOne(ContractType::className(), ['id' => 'contract_type_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }


    /**
     * @inheritdoc
     * @return ContractQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContractQuery(get_called_class());
    }
}
