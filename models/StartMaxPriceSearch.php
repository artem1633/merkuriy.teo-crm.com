<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StartMaxPrice;

/**
 * StartMaxPriceSearch represents the model behind the search form about `app\models\StartMaxPrice`.
 */
class StartMaxPriceSearch extends StartMaxPrice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_start_max_price', 'oz', 'oz_percent', 'oik_mck', 'oik_contract', 'oik_percent', 'ogo_mck', 'ogo_contract', 'ogo_percent', 'contract_id','oik_type','ogo_type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StartMaxPrice::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        $query->joinWith(['bidding'=> function ($q) {
//            $q->andFilterWhere([
//                'bid_sum' => $this->bidSum,
//                'decline' => $this->decline,
//                'product_cost' =>$this->productCoast,
//                'profit' => $this->profit,
//                'profit_percent' => $this->profitPercent
//            ]);
//        }]);

        $query->andFilterWhere([
            'id' => $this->id,
            'contract_start_max_price' => $this->contract_start_max_price,
            'oz' => $this->oz,
            'oz_percent' => $this->oz_percent,
            'oik_mck' => $this->oik_mck,
            'oik_contract' => $this->oik_contract,
            'oik_percent' => $this->oik_percent,
            'ogo_mck' => $this->ogo_mck,
            'ogo_contract' => $this->ogo_contract,
            'ogo_percent' => $this->ogo_percent,
            'contract_id' => $this->contract_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'oik_type' => $this->oik_type,
            'ogo_type' => $this->ogotype
        ]);

        return $dataProvider;
    }
}
