<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CargoShip]].
 *
 * @see CargoShip
 */
class CargoShipQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CargoShip[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CargoShip|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
