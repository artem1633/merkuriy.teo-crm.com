<?php

namespace app\controllers;

use app\models\Calculation;
use app\models\CalculationSearch;
use app\models\CargoShip;
use app\models\CargoShipSearch;
use app\models\CargoStatus;
use app\models\Commission;
use app\models\CommissionSearch;
use app\models\ContractFilter;
use app\models\Control;
use app\models\ControlSearch;
use app\models\CostsSearch;
use app\models\Customer;
use app\models\CustomerContactSearch;
use app\models\CustomerSearch;
use app\models\OrderStatus;
use app\models\OrderStatusSearch;
use app\models\Payments;
use app\models\PaymentsSearch;
use app\models\ProductSearch;
use app\models\ProductSpecificationSearch;
use app\models\StartMaxPrice;
use app\models\StartMaxPriceSearch;
use Yii;
use app\models\Contract;
use app\models\ContractSearch;
use yii\debug\models\timeline\Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        if(isset($_GET['action'])){
            if($_GET['action'] == 'filter'){
                $model = ContractFilter::find()->where(['name' => $searchModel->filterName])->one();
//                if($searchModel->filterId != null){
//                    $model = ContractFilter::findOne($searchModel->filterId);
//                } else {
//                    $model = new ContractFilter();
//                }

                if($model == null){
                    $model = new ContractFilter();
                }

                $model->name = $searchModel->filterName;
                $model->order_status = is_array($searchModel->order_status) ? implode(',', $searchModel->order_status): null;
                $model->delivery_status = is_array($searchModel->deliv_status) ? implode(',', $searchModel->deliv_status): null;
                $model->shortorgname = $searchModel->shortOrgName;
                $model->contract_type_id = is_array($searchModel->contract_type_id) ? implode(',', $searchModel->contract_type_id) : null;
                $model->auction_number = $searchModel->auction_number;
                $model->save(false);

                $searchModel->filterId = $model->id;
            }
        }



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     */
    public function actionParse($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->loadInfo();


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->parsed == 1){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Успех",
                    'content'=>'<span class="text-success">Данные были успешно загружены</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])

                ];
            } else {
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Данные не были загружены",
                    'content'=>'<span class="text-danger">Повторите попытку позже</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * @param string $date
     * @param string $value
     * @return mixed
s     */
    public function actionCalcDate($date, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['date' => date('Y-m-d', strtotime(\app\helpers\WorkDay::getWorkDay($date, $value)))];
    }

    /**
     * @param int $id
     * @param string $attribute
     * @param int $value
     * @return mixed
     */
    public function actionPayToggle($id, $attribute, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->$attribute = $value;
        $model->save(false);
        return ['result' => true];
    }


    /**
     * Displays a single Contract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $costsSearchModel = new CostsSearch();
        $costsDataProvider = $costsSearchModel->search([]);
        $costsDataProvider->query->andWhere(['contract_id' => $id]);

        $calculationSearchModel = Calculation::find()->where(['contract_id' => $id])->one();
//        $calculationDataProvider = $calculationSearchModel->search([]);
//        $calculationDataProvider->query->andWhere(['contract_id' => $id]);

        $controlSearchModel = Control::find()->where(['contract_id' => $id])->one();
//        $controlDataProvider = $controlSearchModel->search([]);
//        $controlDataProvider->query->andWhere(['contract_id' => $id]);

        $orderStatusSearchModel = OrderStatus::find()->where(['contract_id' => $id])->one();
        $cargoStatusSearchModel = CargoStatus::find()->where(['contract_id' => $id])->one();
//        $orderStatusDataProvider = $orderStatusSearchModel->search([]);
//        $orderStatusDataProvider->query->andWhere(['contract_id' => $id]);

        $cargoShipSearchModel = new CargoShipSearch();
        $cargoShipDataProvider = $cargoShipSearchModel->search([]);
        $cargoShipDataProvider->query->andWhere(['contract_id' => $id]);

        $customerContactSearchModel = new CustomerContactSearch();
        $customerContactDataProvider = $customerContactSearchModel->search([]);
        $customerContactDataProvider->query->andWhere(['contract_id' => $id]);

//        $paymentsSearchModel = Payments::find()->where(['contract_id' => $id])->one();
//        $paymentsDataProvider = $paymentsSearchModel->search([]);
//        $paymentsDataProvider->query->andWhere(['contract_id' => $id]);

        $productSearchModel = new ProductSearch();
        $productDataProvider = $productSearchModel->search([]);
        $productDataProvider->query->andWhere(['contract_id' => $id]);

        $customerSearchModel =  Customer::find()->where(['contract_id' => $id])->one();
//        $customerDataProvider = $customerSearchModel->search([]);
//        $customerDataProvider->query->andWhere(['contract_id' => $id]);

        $commissionSearchModel = Commission::find()->where(['contract_id' => $id])->one();
//        $commissionDataProvider = $commissionSearchModel->search([]);
//        $commissionDataProvider->query->andWhere(['contract_id' => $id]);

        $startMaxPriceSearchModel = StartMaxPrice::find()->where(['contract_id' => $id])->one();
//        $startMaxPriceDataProvider = $startMaxPriceSearchModel->search([]);
//        $startMaxPriceDataProvider->query->andWhere(['contract_id' => $id]);

        $specificationSearchModel = new ProductSpecificationSearch();
        $specificationDataProvider = $specificationSearchModel->search([]);
        $specificationDataProvider->query->andWhere(['contract_id' => $id]);


        return $this->render('view', [
            'model' => $this->findModel($id),
            'costsSearchModel' => $costsSearchModel,
            'costsDataProvider' => $costsDataProvider,
            'calculationSearchModel' => $calculationSearchModel,
//            'calculationDataProvider' => $calculationDataProvider,
            'controlSearchModel' => $controlSearchModel,
//            'controlDataProvider' => $controlDataProvider,
            'orderStatusSearchModel' => $orderStatusSearchModel,
            'cargoStatusSearchModel' => $cargoStatusSearchModel,
//            'orderStatusDataProvider' => $orderStatusDataProvider,
            'cargoShipSearchModel' => $cargoShipSearchModel,
            'cargoShipDataProvider' => $cargoShipDataProvider,
            'customerContactSearchModel' => $customerContactSearchModel,
            'customerContactDataProvider' => $customerContactDataProvider,
//            'paymentsSearchModel' => $paymentsSearchModel,
//            'paymentsDataProvider' => $paymentsDataProvider,
            'productSearchModel' => $productSearchModel,
            'productDataProvider' => $productDataProvider,
            'customerSearchModel' => $customerSearchModel,
//            'customerDataProvider' => $customerDataProvider,
            'commissionSearchModel' => $commissionSearchModel,
//            'commissionDataProvider' => $commissionDataProvider,
            'startMaxPriceSearchModel' => $startMaxPriceSearchModel,
//            'startMaxPriceDataProvider' => $startMaxPriceDataProvider,
            'specificationSearchModel' => $specificationSearchModel,
            'specificationDataProvider' => $specificationDataProvider,
        ]);

    }

    /**
     * Creates a new Contract model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Contract();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить контракт",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавление контракта",
                    'content'=>'<span class="text-success">Добавлено успено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Добавить еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить контракт",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить контракт #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                    ];
            }else{
                 return [
                    'title'=> "Изменить контракт #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionDeleteFilter($id)
    {
        $model = ContractFilter::findOne($id);

        if($model){
            $model->delete();
        }

        return $this->redirect(['contract/index']);
    }

     /**
     * Delete multiple existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $errorCount = 0;
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($model->delete() == false){
                $errorCount++;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($errorCount > 0){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Удаление",
                    'content'=>"<span class='text-warning'>Выбранные записи были удалены, кроме {$errorCount} шт. так как стаус заказа не был установлен как <Проиграли></span>",
                    'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
