<?php

namespace app\controllers;

use app\behaviors\RoleBehavior;
use app\helpers\WorkDay;
use app\models\forms\ResetPasswordForm;
use app\models\ReportSettingColumn;
use app\models\Task;
use app\models\User;
use PHPHtmlParser\Dom;
use rmrevin\yii\module\Comments\models\Comment;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDashboard()
    {

        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);

        $user_id = Yii::$app->user->identity->id;
        $tasks = Task::find()->where(['responsible_id' => $user_id])->all();
        foreach ($tasks as $value) {
            $array [] = $value->name;
        }

        $comments = Comment::find()->orderBy(['created_at' => SORT_DESC])->all();

        $comment = [];
        foreach ($comments as $value) {
            $user = User::findOne($value->created_by);
            $str = $value->entity;
            $task_id = preg_replace("/[^0-9]/", '', $str);
            $task = Task::findOne($task_id);


            $name = 'Unknown';
            $task_title = 'Unknown';

            if($task != null) {
                $task_title = $task->name;
            }
            if ($user != null){
                $name = $user->name;
            }
            $comment [] = [
                'user' => $name,
                'title' => $task_title,
                'text' => $value->text,
                'id' => $user->id,
            ];
        }

        return $this->render('dashboard', [
//            'all_tasks' => $result1,
//            'result' => $result,
//             'total_hour' => $total_hour,
            'comment' => $comment,
            'tasks' => $tasks,
        ]);
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    public function actionTest()
    {
//        $dom = new Dom;
//        $html = file_get_html('https://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=0337100007320000174');
//        $html = file_get_html('https://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber=0137200001220006279');
//        $titles = $html->find('.blockInfo__section.section .section__title');
//        $contents = $html->find('.blockInfo__section.section .section__info');
//
//        $data = [];
//
//        for ($i = 0; $i < count($titles); $i++)
//        {
//            $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
//        }
//
//        $titles = $html->find('.cardMainInfo__section .cardMainInfo__title');
//        $contents = $html->find('.cardMainInfo__section .cardMainInfo__content');
//
//
//        for ($i = 0; $i < count($titles); $i++)
//        {
//            $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
//        }
//
//
//        $titles = $html->find('.blockInfo__section .section__title');
//        $contents = $html->find('.blockInfo__section .section__info');
//        for ($i = 0; $i < count($titles); $i++)
//        {
//            $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
//        }
//
//        // Организация
//        $hrefs = $html->find('.cardMainInfo__content a');
//
//        $orgHref = null;
//
//        foreach ($hrefs as $href)
//        {
//            if(strstr($href->getAttribute('href'), 'organizationCode')){
//                $orgHref = $href->getAttribute('href');
//            }
//        }
//
//
//        if($orgHref != null){
//            $html = file_get_html($orgHref);
//
//            $titles = $html->find('.search-results .registry-entry__body .registry-entry__body-title');
//            $contents = $html->find('.search-results .registry-entry__body .registry-entry__body-value');
//
//            for ($i = 0; $i < count($titles); $i++)
//            {
//                $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
//            }
//        }


//        $info = $dom->find('.container');
//        VarDumper::dump($info, 10, true);
//        exit;
//        foreach ($info as $row)
//        {

//        $date = $dom->find('.date');

//            $titles = $dom->find('.cardMainInfo__section .cardMainInfo__title');
//            $contents = $dom->find('.cardMainInfo__content');

//            for ($i = 0; $i < count($titles); $i++)
//            {
//                $data[$titles[$i]->innerHtml] = $contents[$i]->innerText;
//            }
//        }


//        $header = $dom->find('#header-title-site');


        echo str_replace(',', '.', str_replace(' ', '', '2 296 640,00'));


//        return $html;
//        return $dom->outerHTML;

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
