<?php

namespace app\controllers;

use app\models\Calculation;
use app\models\CalculationSearch;
use app\models\CargoShip;
use app\models\CargoShipSearch;
use app\models\CargoStatus;
use app\models\Commission;
use app\models\CommissionSearch;
use app\models\ContractFilter;
use app\models\Control;
use app\models\ControlSearch;
use app\models\CostsSearch;
use app\models\Customer;
use app\models\CustomerContactSearch;
use app\models\CustomerSearch;
use app\models\OrderStatus;
use app\models\OrderStatusSearch;
use app\models\Payments;
use app\models\PaymentsSearch;
use app\models\ProductSearch;
use app\models\ProductSpecificationSearch;
use app\models\StartMaxPrice;
use app\models\StartMaxPriceSearch;
use Yii;
use app\models\Contract;
use app\models\ContractSearch;
use yii\debug\models\timeline\Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
