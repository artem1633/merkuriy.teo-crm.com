<?php

namespace app\helpers;
use app\models\Weekend;

/**
 * Class WorkDay
 * @package app\helpers
 */
class WorkDay
{
    /**
     * @param string $startDate
     * @param int $plusDays
     * @return string
     */
    public static function getWorkDay($startDate, $plusDays)
    {
        $continue = false;
        $plusDate = date('Y-m-d H:i:s', (strtotime($startDate) + 86400 * $plusDays));

        if(self::isWeekend($plusDate)){
            $continue = true;
        } else {
            $plusMonth = intval(date('m', strtotime($plusDate)));
            $plusDay = intval(date('d', strtotime($plusDate)));
            $weekend = Weekend::find()->where(['month' => $plusMonth, 'day' => $plusDay])->one();

            if($weekend){
                $continue = true;
            }
        }

        if($continue){
            $date = self::getWorkDay(date('Y-m-d H:i:s', (strtotime($startDate) + 86400)), $plusDays);
            return $date;
        } else {
            return $plusDate;
        }
    }

    /**
     * @param string $date
     * @return bool
     */
    private static function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }
}