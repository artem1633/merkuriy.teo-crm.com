<?php

namespace app\helpers;
use PHPHtmlParser\Dom;

/**
 * Class ZakupkiParser
 * @package app\helpers
 */
class ZakupkiParser
{
    /**
     * @param $regNumber
     * @return array|boolean
     */
    public static function parse($regNumber)
    {
        $data = [];

        try {
//            $dom = new Dom;
//            $html = file_get_contents('https://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber='.$regNumber);
//            $dom->loadStr($html);
//            $info = $dom->find('.blockInfo__section.section');
//
//            foreach ($info as $row)
//            {
//                $titles = $row->find('.section__title');
//                $contents = $row->find('.section__info');
//
//                for ($i = 0; $i < count($titles); $i++)
//                {
//                    $data[$titles[$i]->innerHtml] = $contents[$i]->innerText;
//                }
//            }

            $html = file_get_html('https://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber='.$regNumber);
            $titles = $html->find('.blockInfo__section.section .section__title');
            $contents = $html->find('.blockInfo__section.section .section__info');

            $data = [];

            for ($i = 0; $i < count($titles); $i++)
            {
                $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
            }


            $titles = $html->find('.cardMainInfo__section .cardMainInfo__title');
            $contents = $html->find('.cardMainInfo__section .cardMainInfo__content');

            for ($i = 0; $i < count($titles); $i++)
            {
                $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
            }

            $titles = $html->find('.blockInfo__section .section__title');
            $contents = $html->find('.blockInfo__section .section__info');
            for ($i = 0; $i < count($titles); $i++)
            {
                $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
            }

            // Организация
            $hrefs = $html->find('.cardMainInfo__content a');

            $orgHref = null;

            foreach ($hrefs as $href)
            {
                if(strstr($href->getAttribute('href'), 'organizationCode')){
                    $orgHref = $href->getAttribute('href');
                }
            }


            if($orgHref != null){
                $html = file_get_html($orgHref);

                $titles = $html->find('.search-results .registry-entry__body .registry-entry__body-title');
                $contents = $html->find('.search-results .registry-entry__body .registry-entry__body-value');

                for ($i = 0; $i < count($titles); $i++)
                {
                    $data[$titles[$i]->innertext] = trim(strip_tags($contents[$i]->innertext));
                }
            }


        } catch (\Exception $e) {
            \Yii::warning($e->getMessage(), 'Error while parsing');
            \Yii::$app->session->setFlash('error', 'Во время парсинга произошла ошибка. Проверьте существует ли такой лот на сайте гос закупок или повторите попытку через 10-15 секунд');
            return false;
        }

        \Yii::$app->session->setFlash('success', 'Информация успешно загружена');

        return $data;
    }

}