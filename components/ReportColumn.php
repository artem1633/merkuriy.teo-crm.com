<?php

namespace app\components;

use app\models\AmsType;
use app\models\City;
use app\models\Contract;
use app\models\Customer;
use app\models\Order;
use app\models\OrderStatus;
use app\models\OrderWorkType;
use app\models\Placement;
use app\models\TaskStatus;
use app\models\User;
use app\models\WorkType;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class ReportColumn
 * @package app\components
 */
class ReportColumn extends Component
{
    const DATE_COLUMNS = [
        'contract_contract_date',
        'task_created_at',
        'task_updated_at',
        'order_date',
        'fact_end_date',
        'order_deadline',
        'created_at',
        'updated_at',
        'email_date',
        'email_matching_date',
        'edo_date',
        'edo_getting_date',
        'contract_contract_date',
        'contract_created_at',
        'contract_updated_at'
    ];

    /**
     * @var array
     */
    public $columns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

//        if($this->columns == null || count($this->columns) == 0){
//            throw new InvalidConfigException('$columns must be required');
//        }
    }

    /**
     * @return array
     */
    public function getGridColumns()
    {
        for ($i = 0; $i < count($this->columns); $i++){
            $column = $this->columns[$i];

            if($column['attribute'] == 'contract_city_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(City::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $city = City::findOne($model['contract_city_id']);
                    if($city != null){
                        return $city->name;
                    }
                };
            } else if($column['attribute'] == 'contract_customer_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Customer::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $customer = Customer::findOne($model['contract_customer_id']);
                    if($customer != null){
                        return $customer->name;
                    }
                };
            } else if($column['attribute'] == 'contract_author_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['contract_author_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'author_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['author_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'city_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(City::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $city = City::findOne($model['city_id']);
                    if($city != null){
                        return $city->name;
                    }
                };
            } else if($column['attribute'] == 'contract_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Contract::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $contract = Contract::findOne($model['contract_id']);
                    if($contract != null){
                        return $contract->name;
                    }
                };
            } else if($column['attribute'] == 'order_status_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $status = OrderStatus::findOne($model['order_status_id']);
                    if($status != null){
                        return $status->name;
                    }
                };
            } else if($column['attribute'] == 'ams_type_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(AmsType::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $type = AmsType::findOne($model['ams_type_id']);
                    if($type != null){
                        return $type->name;
                    }
                };
            } else if($column['attribute'] == 'placement_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Placement::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $placement = Placement::findOne($model['placement_id']);
                    if($placement != null){
                        return $placement->name;
                    }
                };
            } else if($column['attribute'] == 'task_author_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['task_author_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'task_order_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Order::find()->all(), 'id', 'order_number');
                $this->columns[$i]['value'] = function($model){
                    $order = Order::findOne($model['task_order_id']);
                    if($order != null){
                        return $order->order_number;
                    }
                };
            } else if($column['attribute'] == 'task_task_status_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(TaskStatus::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $status = TaskStatus::findOne($model['task_task_status_id']);
                    if($status != null){
                        return $status->name;
                    }
                };
            } else if($column['attribute'] == 'task_responsible_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['task_responsible_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'task_checker_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['task_checker_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'task_normocontroller_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['task_normocontroller_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'task_gip_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $user = User::findOne($model['task_gip_id']);
                    if($user != null){
                        return $user->name;
                    }
                };
            } else if($column['attribute'] == 'workTypes'){
                $this->columns[$i]['filter'] = ArrayHelper::map(WorkType::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $types = ArrayHelper::getColumn(WorkType::findAll(ArrayHelper::getColumn(OrderWorkType::find()->where(['order_id' => $model['order_id']])->all(), 'work_type_id')), 'name');
                    return implode(', ', $types);
                };
            } else if(in_array($column['attribute'], self::DATE_COLUMNS)){
                $this->columns[$i]['filterInputOptions'] = ['class' => 'form-control', 'type' => 'date'];
            }
        }

//        VarDumper::dump($this->columns, 10, true);
//        exit;

        return $this->columns;
    }
}